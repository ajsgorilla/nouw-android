package com.nouw.app;

import android.app.Application;

public class BaseApplication extends Application {

    private static BaseApplication instance;

    public BaseApplication() {
        super();
        instance = this;
    }

    public static BaseApplication getInstance() {
        return instance;
    }
}
