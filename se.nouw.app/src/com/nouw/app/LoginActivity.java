package com.nouw.app;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Locale;

import org.apache.http.client.methods.HttpGet;

import com.facebook.*;
import com.facebook.model.GraphUser;
import com.nouw.app.BasicParsers.StringParser;
import com.nouw.app.NattstadWebserviceCall.ErrorCode;
import com.nouw.app.NattstadWebserviceCall.ResultCallback;

import com.nouw.app.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class LoginActivity extends Activity {
	public static final String LOGIN_NICKNAME_RESULT = "se.nattstad.login_nickname_result";
	public static final String USERNAME_KEY = "se.nattstad.username";
	public static final String PASSWORD_KEY = "se.nattstad.password";
	public static final String NICKNAME_KEY = "se.nattstad.nickname";
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        
        Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
        TextView title = (TextView) findViewById(R.id.login_title);
        title.setTypeface(typeface);
        
        TextView createABlog = (TextView) findViewById(R.id.login_create_a_blog);
        createABlog.setTypeface(typeface);
        
        EditText username = (EditText) findViewById(R.id.login_username);
        username.setTypeface(typeface);
        
        EditText password = (EditText) findViewById(R.id.login_password);
        password.setTypeface(typeface);
        
		Button loginButton = (Button)findViewById(R.id.login_button);
		loginButton.setTypeface(typeface);
		loginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				normalLogin();
			}
		});
		
		Button fbLoginButton = (Button)findViewById(R.id.fb_login_button);
		fbLoginButton.setTypeface(typeface);
		fbLoginButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				facebookLogin();
			}
		});
		
		Button createBlogButton = (Button) findViewById(R.id.create_blog_button);
		createBlogButton.setTypeface(typeface);
		createBlogButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View arg0) {
				createBlog();
			}
		});
		
		Button forgotPasswordButton = (Button) findViewById(R.id.forgot_password_button);
		forgotPasswordButton.setTypeface(typeface);
		forgotPasswordButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				forgotPassword();
			}
		});
		
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		Log.i("LoginActivity", "onDestory");
	}
	
	private ResultCallback<String> normalLoginCallback = new ResultCallback<String>() {
		@Override
		public void handleResult(String result, Exception exception) {
			if (exception != null || result == null) {
				if (exception != null && exception instanceof ErrorCode) {
					int statusCode = ((ErrorCode)exception).getErrorCode();
					if (statusCode == 401) {
						new AlertDialog.Builder(LoginActivity.this).setTitle(getResources().getString(R.string.login_failed_msg))
						.setMessage(getResources().getString(R.string.wrong_cred_msg))
						.setPositiveButton(android.R.string.ok, null)
						.show();
					}
					else if (statusCode == 503) {
						new AlertDialog.Builder(LoginActivity.this).setTitle(getResources().getString(R.string.login_failed_msg))
						.setMessage(getResources().getString(R.string.maintenance_in_progress_msg))
						.setPositiveButton(android.R.string.ok, null)
						.show();
					}
					else {
						new AlertDialog.Builder(LoginActivity.this).setTitle(getResources().getString(R.string.login_failed_msg))
						.setMessage(getResources().getString(R.string.nattstad_unreachable))
						.setPositiveButton(android.R.string.ok, null)
						.show();
					}
				}
				else {
					new AlertDialog.Builder(LoginActivity.this).setTitle(getResources().getString(R.string.login_failed_msg))
					.setMessage(getResources().getString(R.string.nattstad_unreachable))
					.setPositiveButton(android.R.string.ok, null)
					.show();
				}
			}
			else {
				Log.i("login_activity", "normal login successful as " + result);
				String username = ((EditText)findViewById(R.id.login_username)).getText().toString();
				String password = ((EditText)findViewById(R.id.login_password)).getText().toString();
				
				SharedPreferences prefs = getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, MODE_PRIVATE);
				Editor editor = prefs.edit();
				editor.putString(USERNAME_KEY, username);
				editor.putString(PASSWORD_KEY, password);
				editor.putString(NICKNAME_KEY, result);
				editor.commit();
				
				Intent loginIntent = new Intent(LoginActivity.this, MainActivity.class);
	            startActivity(loginIntent);
	            finish();
			}
		}
	};
	
	private ResultCallback<String> facebookLoginResult = new ResultCallback<String>() {
		@Override
		public void handleResult(String result, Exception exception) {
			if (exception != null || result == null) {
				if (exception != null && exception instanceof ErrorCode) {
					int statusCode = ((ErrorCode)exception).getErrorCode();
					if (statusCode == 503) {
						new AlertDialog.Builder(LoginActivity.this).setTitle(getResources().getString(R.string.maintenance))
						.setMessage(getResources().getString(R.string.maintenance_in_progress_msg))
						.setPositiveButton(android.R.string.ok, null)
						.show();
					}
					else {
						new AlertDialog.Builder(LoginActivity.this).setTitle("Inloggningen misslyckades")
						.setMessage(getResources().getString(R.string.account_not_connected_to_fb_msg))
						.setPositiveButton(android.R.string.ok, null)
						.show();
					}
				}
				else {
					new AlertDialog.Builder(LoginActivity.this).setTitle("Inloggningen misslyckades")
					.setMessage("Nattstad kan inte n�s just nu, f�rs�k g�rna senare.")
					.setPositiveButton(android.R.string.ok, null)
					.show();
				}
			}
			else {
				Log.i("login_activity", "facebook login successful as " + result);
				Editor editor = getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, 0).edit();
				editor.putString(NICKNAME_KEY, result);
				editor.commit();
				
				Intent loginIntent = new Intent(LoginActivity.this, MainActivity.class);
	            startActivity(loginIntent);
	            finish();
			}
		}
	};
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(this, requestCode, resultCode, data);
    }
	
	private void normalLogin() {
		EditText username = (EditText)findViewById(R.id.login_username);
		EditText password = (EditText)findViewById(R.id.login_password);
		
		try {
			HttpGet get = NattstadWebserviceCall.newGetRequest(NattstadWebserviceCall.Method.CheckUserLogin, 
															      username.getText().toString(), 
															      password.getText().toString(), 
															      "", 
															      null);
			new NattstadWebserviceCall<String>(new StringParser(), normalLoginCallback).execute(get);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	private void facebookLogin() {
		Session.openActiveSession(LoginActivity.this, true, new Session.StatusCallback() {
			@Override
			public void call(Session session, SessionState state, Exception exception) {
				if (session.isOpened()) {
					Request.newMeRequest(session, new Request.GraphUserCallback() {
						// callback after Graph API response with user object
						@Override
						public void onCompleted(GraphUser user, Response response) {
							if (user != null) {
								String accessToken = Session.getActiveSession().getAccessToken();
								HttpGet get = null;
								try {
									get = NattstadWebserviceCall.newGetRequest(NattstadWebserviceCall.Method.CheckFacebookUserLogin, "", "", accessToken, null);
								} 
								catch (MalformedURLException e) {
									e.printStackTrace();
								}
								catch (URISyntaxException e) {
									e.printStackTrace();
								}
								new NattstadWebserviceCall<String>(new StringParser(), facebookLoginResult).execute(get);
							}
							else {
								FacebookRequestError error = response.getError();
								
								new AlertDialog.Builder(LoginActivity.this).setTitle("Kan inte logga in med Facebook")
								.setMessage(error.getErrorMessage())
								.setPositiveButton(android.R.string.ok, null)
								.show();
							}
						}
					}).executeAsync();
				}
				else {
					if (exception != null) {
						
						new AlertDialog.Builder(LoginActivity.this).setTitle("Kan inte logga in med Facebook")
						.setMessage(exception.getMessage())
						.setPositiveButton(android.R.string.ok, null)
						.show();
					}
				}
			}
		});
	}
	
	private void createBlog() {
		Intent createBlog = new Intent(this, SimpleWebViewActivity.class);
		createBlog.putExtra(SimpleWebViewActivity.START_URL, WebViewFragment.WEBVIEW_BASE_URL + "register.aspx?app=true&lang_code="+Locale.getDefault().getLanguage());
		startActivity(createBlog);
	}
	
	private void forgotPassword() {
		Intent forgotPassword = new Intent(this, SimpleWebViewActivity.class);
		forgotPassword.putExtra(SimpleWebViewActivity.START_URL, WebViewFragment.WEBVIEW_BASE_URL + "passwordReminder.aspx?app=true&lang_code="+Locale.getDefault().getLanguage());
		startActivity(forgotPassword);
	}
	
	
}
