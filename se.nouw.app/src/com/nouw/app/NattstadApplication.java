package com.nouw.app;

import android.app.Application;
import android.os.AsyncTask;


public class NattstadApplication extends Application {
	@Override
	public void onCreate() {
		super.onCreate();
		
		// this is a workaround for a bug where asynctask is loaded on another thread than the main thread and
		// later gives a NoClassDefFoundError exception...
		new AsyncTask<Void, Void, Void>() {
			@Override
			protected Void doInBackground(Void... params) {
				return null;
			}
		};
	}
	
	private static NattstadApplication instance;

    public NattstadApplication() {
        super();
        instance = this;
    }

    public static NattstadApplication getInstance() {
        return instance;
    }
}
