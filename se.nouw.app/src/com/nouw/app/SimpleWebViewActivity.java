package com.nouw.app;

import com.nouw.app.R;
import android.annotation.SuppressLint;
import android.app.Activity;
import android.os.Bundle;
import android.webkit.WebView;

public class SimpleWebViewActivity extends Activity {
	public static final String START_URL = "se.nattstad.simple_webview_activity_start_url";
	
	@SuppressLint("SetJavaScriptEnabled")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_create_blog);
		
		Bundle extras = getIntent().getExtras();
		
		if (extras != null) {
			WebView webview = (WebView) findViewById(R.id.create_blog_webview);
			webview.getSettings().setJavaScriptEnabled(true);
			webview.loadUrl(extras.getString(START_URL));
		}
	}
}
