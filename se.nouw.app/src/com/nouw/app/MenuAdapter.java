package com.nouw.app;

import java.util.List;

import com.nouw.app.R;

import com.facebook.Session;
import com.nouw.app.SideMenuItem.MenuOption;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class MenuAdapter extends ArrayAdapter<SideMenuItem> {
	
	private List<SideMenuItem> menuItems;
	private LayoutInflater layoutInflater;
	private int numUnreadMessages = 0;
	private Context context;
	
	public MenuAdapter(Context context, int resource, List<SideMenuItem> menuItems, int numUnreadMessages) {
		super(context, resource, menuItems);
		this.context = context;
		this.menuItems = menuItems;
		layoutInflater = LayoutInflater.from(context);
		this.numUnreadMessages = numUnreadMessages;
	}
	
	@Override
	public SideMenuItem getItem(int position) {
		return menuItems.get(position);
	}
	
	@Override
	public int getCount() {
		return menuItems.size();
	}
	
	@Override
	public View getView(int position, View convertView, ViewGroup parent) {
		SideMenuItem menuItem = menuItems.get(position);
		if (menuItem.option != MenuOption.None) { 
			View view = layoutInflater.inflate(R.layout.side_menu_row, parent, false);

			ImageView imageView = (ImageView) view.findViewById(R.id.menu_image);
			int resourceId = 0;
			switch (menuItem.option) {
			case Inbox: resourceId = R.drawable.inbox; break;
			case Newsfeed: resourceId = R.drawable.newsfeed; break; 
			case Magazine: resourceId = R.drawable.magazineicon; break; 
			case Following: resourceId = R.drawable.following; break; 
			case AllBlogPosts: resourceId = R.drawable.allblogposts; break; 
			case Comments: resourceId = R.drawable.magazineicon; break;
			case Settings: resourceId = R.drawable.settings; break;
			case BlogDesign: resourceId = R.drawable.designicon; break; 
			case Statistics: resourceId = R.drawable.statistics; break;
			case BlogTopList: resourceId = R.drawable.toplist; break; 
			case MyProfile: resourceId = R.drawable.profile; break; 
			case MyBlog: resourceId = R.drawable.myblog; break; 
			case MyPictures: resourceId = R.drawable.pictures; break;
			case AccountInfo: resourceId = R.drawable.settings; break; 
			case NewBlogPost: resourceId = R.drawable.newblogpost; break;
			case Blocklist: resourceId = R.drawable.ban; break;
			case LogOut: resourceId = R.drawable.logout; break;
			}

			imageView.setImageResource(resourceId);

			TextView title = (TextView) view.findViewById(R.id.menu_title);
			title.setText(menuItem.title);
			
			TextView numUnread = (TextView) view.findViewById(R.id.menu_num_unread);
			String unreadString = "";
			if (numUnreadMessages > 0 && menuItem.option == MenuOption.Inbox) {
				unreadString = Integer.toString(numUnreadMessages);
			}
			numUnread.setText(unreadString);
			
			return view;
		}
		else {
			TextView view = (TextView) layoutInflater.inflate(R.layout.side_menu_section_header, parent, false);
			String title = menuItem.title;
			if (title.equals("SMEKNAMN")) {
				title = context.getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, 0).getString(LoginActivity.NICKNAME_KEY, "SMEKNAMN").toUpperCase();
			}
			view.setText(title);
			return view;
		}
	}
}
