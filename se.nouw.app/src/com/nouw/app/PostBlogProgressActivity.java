package com.nouw.app;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URISyntaxException;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.UnrecoverableKeyException;
import java.util.Iterator;
import java.util.Set;

import javax.xml.parsers.ParserConfigurationException;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EncodingUtils;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import com.nouw.app.NattstadWebserviceCall.Method;

import com.nouw.app.R;

import android.app.Activity;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Looper;
import android.util.Log;
import android.view.View;
import android.widget.TextView;

public class PostBlogProgressActivity extends Activity {
	private static final int DEFAULT_IMAGE_HEIGHT = 1400;
	private static final int DEFAULT_IMAGE_WIDTH = 940;
	private static ByteArrayOutputStream jpegBytes = new ByteArrayOutputStream(); 
	private int uploadImageWidth = DEFAULT_IMAGE_WIDTH;
	private int uploadImageHeight = DEFAULT_IMAGE_HEIGHT;

	private class UploadTask extends AsyncTask<Bundle, Void, Void>
	{
		private Bundle state	= null;

		private int postBlogBody() throws KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, URISyntaxException, Exception {
			Bundle b = getIntent().getExtras();
			Bundle postBlogBody = b.getBundle("postBlogBody");

			if(postBlogBody != null) {
				return sendHTTPPostRequest(postBlogBody, Method.PostBlog);
			}

			return 0;
		}

		private void postBlogPictures(int blogID) throws KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, URISyntaxException, Exception {
			Bundle b = getIntent().getExtras();
			Bundle postBlogBody = b.getBundle("postBlogBody");
			Bundle postPictureBody = b.getBundle("postPictureBody");
			if(postPictureBody != null) {
				int numImages = postPictureBody.getInt("numImages");
				for(int i = 0; i < numImages; ++i) {
					String imageKey = "image" + Integer.toString(i);
					boolean previousSendSucceeded = state.getBoolean(imageKey);
					if(!previousSendSucceeded) {
						sendBlogPicture(blogID, postPictureBody.getBundle(imageKey), i == (numImages - 1), i + 1, postBlogBody.getString("username"), 
										postBlogBody.getString("password"), postBlogBody.getString("facebooktoken"), numImages);
						state.putBoolean(imageKey, true);
					}
				}
			}
			runOnUiThread(new Runnable() { public void run() {
				Intent intent = getIntent();
				setResult(RESULT_OK, intent);
				finish();
			}});
		}


		@Override
		protected Void doInBackground(Bundle... states) {
			try {
				state = states[0];
				int blogID 		= 0; 
				if(!state.getBoolean("postBlogBodySucceeded")) {
					blogID 		= postBlogBody();
					state.putBoolean("postBlogBodySucceeded", true);
					state.putInt("blogID", blogID);
				} else {
					blogID		= state.getInt("blogID");
				}
				if (Looper.myLooper() == null) {
					Looper.prepare();
				}
				postBlogPictures(blogID);
			} catch(final Exception e) {
				runOnUiThread(new Runnable() { public void run() {
					String msg = e.getMessage();
					Utility.showRetryDialog(PostBlogProgressActivity.this, 
							msg, 
							new DialogInterface.OnClickListener() {
						public void onClick(DialogInterface dialog, int which) {
							new UploadTask().execute(state);
						} 
					}, 
					new Dialog.OnCancelListener() {
						public void onCancel(DialogInterface dialog) {
							finish();
						}
					});
				}});
			}
			return null;
		}
	}    

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.post_blog_progress);
		TextView loadingText = (TextView)findViewById(R.id.loadingText);
		loadingText.setVisibility(View.GONE);
		
		calculateImageSize(!Connectivity.isConnectedFast(this));
		new UploadTask().execute(new Bundle());
	}

	private void calculateImageSize(boolean slowConnection) {
		Bundle b = getIntent().getExtras();
		int normalWidth = b.getInt("normalWidth");
		int slowNetworkWidth = b.getInt("slowNetworkWidth");
		
		if (normalWidth == 0 || slowNetworkWidth == 0) return;
		
		uploadImageWidth = slowConnection ? slowNetworkWidth : normalWidth;
		uploadImageHeight = (int) (uploadImageWidth * ((float)DEFAULT_IMAGE_HEIGHT / DEFAULT_IMAGE_WIDTH));
	}

	private void sendBlogPicture(int blogId, Bundle b, boolean lastPicture, final int pictureNumber, String username, String password, String accessToken, final int numImages) throws KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, URISyntaxException, Exception {
		Bundle data = new Bundle();
//		data.putString("android", "true");
		data.putString("lastPicture", lastPicture ? "true" : "false");
		data.putString("pic_number", Integer.toString(pictureNumber));
		data.putString("BlogID", Integer.toString(blogId));
		data.putString("FolderId", Integer.toString(0));
		data.putString("size1", b.getString("size"));
		Uri imageUri = b.getParcelable("uri");
		int orientation = Utility.getOrientationOfImage(this, imageUri);
		if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
			data.putString("orientation", Integer.toString(ExifInterface.ORIENTATION_ROTATE_270));
		}
		else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
			data.putString("orientation", Integer.toString(ExifInterface.ORIENTATION_ROTATE_90));
		}
		else {
			data.putString("orientation", Integer.toString(orientation));
		}
		if (orientation == ExifInterface.ORIENTATION_ROTATE_90 || orientation == ExifInterface.ORIENTATION_ROTATE_270) {
			data.putByteArray("upload" + Integer.toString(pictureNumber) + ".jpeg", Utility.createJPEG(this, imageUri, uploadImageHeight, uploadImageWidth));
		}
		else {
			data.putByteArray("upload" + Integer.toString(pictureNumber) + ".jpeg", Utility.createJPEG(this, imageUri, uploadImageWidth, uploadImageHeight));
		}

		runOnUiThread(new Runnable() { public void run() {
			TextView loadingText = (TextView)findViewById(R.id.loadingText);
			loadingText.setVisibility(View.VISIBLE);
			loadingText.setText(getResources().getString(R.string.uploading_image)+" "+Integer.toString(pictureNumber)+" "+getResources().getString(R.string.of)+" "+Integer.toString(numImages) + "...");
		}});
		
		HttpPost post = NattstadWebserviceCall.newPostRequest(this, data, Method.PostPicture);
		boolean success = NattstadWebserviceCall.call(post, new BasicParsers.BoolParser());
	}

	private int sendHTTPPostRequest(Bundle b, Method method) throws KeyManagementException, UnrecoverableKeyException, NoSuchAlgorithmException, KeyStoreException, URISyntaxException, Exception {
		HttpPost post = NattstadWebserviceCall.newPostRequest(this, b, method);
		int blogId = NattstadWebserviceCall.call(post, new BasicParsers.IntParser());
		return blogId;
	}

	@Override
	protected void onStart() {
		super.onStart();
		// The activity is about to become visible.
	}
	@Override
	protected void onResume() {
		super.onResume();
		// The activity has become visible (it is now "resumed").
	}
	@Override
	protected void onPause() {
		super.onPause();
		// Another activity is taking focus (this activity is about to be "paused").
	}
	@Override
	protected void onStop() {
		super.onStop();
		// The activity is no longer visible (it is now "stopped")
	}
	@Override
	protected void onDestroy() {
		super.onDestroy();
		// The activity is about to be destroyed.
	}
}
