package com.nouw.app;

import java.util.ArrayList;
import java.util.List;

import com.nouw.app.R;

import android.app.Application;
import android.content.Context;
import android.content.res.Resources;
import android.provider.ContactsContract.Contacts.Data;

public class SideMenuItem{
	public static enum MenuOption {
		Inbox,
		Newsfeed,
		Magazine,
		Following,
		NewBlogPost,
		AllBlogPosts,
		Comments,
		Settings,
		BlogDesign,
		Statistics,
		BlogTopList,
		MyProfile,
		MyBlog,
		MyPictures,
		AccountInfo,
		Blocklist,
		LogOut,
		Notifications,
		None
	}
	private static final int NUM_NOT_PRESENT_IN_MENU = 2;
//	private static final String[] menuOptionStrings = new String[] {
//		"Meddelanden",
//		"Nyheter",
//		"Magazine",
//		"F�ljer/S�k",
//		"Nytt inl�gg",
//		"Alla inl�gg",
//		"Kommentarer",
//		"Inst�llningar",
//		"Design",
//		"Statistik",
//		"Bloggtopplistan",
//		"Min profil",
//		"Min blogg",
//		"Bilder",
//		"Kontouppgifter",
//		"Blokkeringer",
//		"Logga ut",
//		"Notifieringar",
//		""
//	};
	private  String[] menuOptionStrings = NattstadApplication.getInstance().getResources().getStringArray(R.array.menu_item);
	public static final MenuOption[] menuOptions = MenuOption.values();
	
	public static String menuOptionToUrl(MenuOption option) {
		switch (option) {
		case Inbox: return "own_messeges.aspx"; // yes, it's misspelled
		case Newsfeed: return "own_news.aspx";
		case Magazine: return "magazineread.aspx";
		case Following: return "own_friends.aspx";
		case NewBlogPost: return null;
		case AllBlogPosts: return "own_blog_posts.aspx";
		case Comments: return "own_blog_comments.aspx";
		case Settings: return "own_blog_settings.aspx";
		case BlogDesign: return "own_blog_design2013.aspx";
		case Statistics: return "own_visits.aspx";
		case BlogTopList: return "own_blogg_top.aspx";
		case MyProfile: return "visit_presentation.aspx?id=%d";
		case MyBlog: return "%s";
		case MyPictures: return "own_pictures.aspx";
		case AccountInfo: return "own_info.aspx";
		case LogOut: return null;
		case Blocklist: return "own_blocked.aspx";
		case Notifications: return "own_notifications.aspx";
		default: return null;
		}
	}
	
	public  String menuOptionToString(MenuOption option) {
		return menuOptionStrings[option.ordinal()];
	}
	
	public final MenuOption option;
	public final String title;
	
	public SideMenuItem(int id) {
		option = menuOptions[id];
		title = menuOptionStrings[id];
	}
	
	public SideMenuItem(MenuOption option, String title) {
		this.option = option;
		this.title = title;
	}

	public static List<SideMenuItem> getItemList() {
		List<SideMenuItem> menuItems = new ArrayList<SideMenuItem>();
		for (int i = 0; i < menuOptions.length - NUM_NOT_PRESENT_IN_MENU; ++i) {
			menuItems.add(new SideMenuItem(i));
		}
		
		Resources res = NattstadApplication.getInstance().getResources();
		
		menuItems.add(0, new SideMenuItem(MenuOption.None, res.getString(R.string.menu)));
		menuItems.add(5, new SideMenuItem(MenuOption.None, res.getString(R.string.your_blog)));
		menuItems.add(13, new SideMenuItem(MenuOption.None, "SMEKNAMN"));
		return menuItems;
	}

//	public Context getContext() {
//		return context;
//	}
//
//	public void setContext(Context context) {
//		this.context = context;
//	}
//	
}
