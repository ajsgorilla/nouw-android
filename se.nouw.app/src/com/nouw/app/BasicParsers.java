package com.nouw.app;

import java.io.IOException;
import java.io.InputStream;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import com.nouw.app.NattstadWebserviceCall.ResultParser;


import android.util.Xml;

public class BasicParsers {
	
	public static class StringParser implements ResultParser<String> {
    	@Override
    	public String parseResult(InputStream in) throws Exception {
    		return BasicParsers.parseString(in);
    	}
    }
	
	public static class IntParser implements ResultParser<Integer> {
		@Override
		public Integer parseResult(InputStream in) throws Exception {
			return parseInt(in);
		}
	}
	
	public static class BoolParser implements ResultParser<Boolean> {
		@Override
		public Boolean parseResult(InputStream in) throws Exception {
			return parseBool(in);
		}
	}
	
	public static String parseString(InputStream in) throws XmlPullParserException, IOException {
		return parseSimpleXml(in, "string");
	}
	
	public static int parseInt(InputStream in) throws XmlPullParserException, IOException {
		try {
			return Integer.parseInt(parseSimpleXml(in, "int"));
		}
		catch (NumberFormatException e) {
			return 0;
		}
	}
	
	public static boolean parseBool(InputStream in) throws XmlPullParserException, IOException {
		return Boolean.parseBoolean(parseSimpleXml(in, "boolean"));
	}
	
	private static String parseSimpleXml(InputStream in, String root) throws XmlPullParserException, IOException {
		XmlPullParser parser = Xml.newPullParser();
		parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(in, null);
        parser.nextTag();
        parser.require(XmlPullParser.START_TAG, null, root);
        parser.next();
        parser.require(XmlPullParser.TEXT, null, null);
        return parser.getText();
	}
}
