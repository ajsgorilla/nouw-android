package com.nouw.app;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;
import java.security.UnrecoverableKeyException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Locale;
import java.util.Set;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManager;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpRequestBase;
import org.apache.http.client.utils.URIUtils;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.conn.ClientConnectionManager;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EncodingUtils;

import com.facebook.Session;

import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;

public class NattstadWebserviceCall<Result> extends AsyncTask<HttpRequestBase, Void, Result> {
	//public static final String BASE_URL = "www.nattstad.se/webserviceiphone2013.asmx";
//	public static final String BASE_URL = "dev2.nattstad.se/webserviceiphone2013.asmx";
	public static final String BASE_URL = "nouw.com/webserviceiphone2013.asmx";
	public static final String URL_SCHEME = "https";
	
	// these all match webservice calls, has to be the exact same name
	public enum Method {
		CheckUserLogin,
		CheckFacebookUserLogin,
		GetCountNewNotifications,
		GetCountNewMessages,
		GetBlogCategories,
		GetUsersbyName,
		GetFollow,
		SetFollowOn,
		SetFollowOff,
		RegisterAndroidToken,
		UnregisterAndroidToken,
		GetImageUploadWidth,
		PostBlog,
		PostPicture,
		PostPictureAndroid
	}
	
	public static HttpGet newGetRequest(Context context, Method method, List<BasicNameValuePair> otherParams) throws MalformedURLException, URISyntaxException {
		SharedPreferences preferences = context.getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, 0);
		if(otherParams == null)
		{
			otherParams = new ArrayList<BasicNameValuePair>();
		}
		
		/*
		if(!otherParams.contains("lang_code"))
		{
			otherParams.add(new BasicNameValuePair("lang_code", Locale.getDefault().getLanguage()));
		}
		*/
		
		String username = preferences.getString(LoginActivity.USERNAME_KEY, "");
		String password = preferences.getString(LoginActivity.PASSWORD_KEY, "");
		Session session = Session.getActiveSession();
		String accessToken = "";
		if (session != null) {
			accessToken = session.getAccessToken();
			if (accessToken == null) accessToken = "";
		}
		
		return newGetRequest(method, username, password, accessToken, otherParams);
	}
	
	public static HttpGet newGetRequest(Method method, String username, String password, String facebooktoken, List<BasicNameValuePair> otherParams) throws URISyntaxException, MalformedURLException {
		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
		params.add(new BasicNameValuePair("username", username));
		params.add(new BasicNameValuePair("password", password));
		params.add(new BasicNameValuePair("facebooktoken", facebooktoken));
		params.add(new BasicNameValuePair("lang_code", Locale.getDefault().getLanguage()));
		if (otherParams != null) params.addAll(otherParams);
		
		URI uri = URIUtils.createURI(URL_SCHEME, BASE_URL, -1, method.toString(), 
									 URLEncodedUtils.format(params, "utf-8"), null);
		return new HttpGet(uri);
	}
	
	public static interface ResultParser<Result> {
		Result parseResult(InputStream in) throws Exception;
	}
	
	public static interface ResultCallback<Result> {
		void handleResult(Result result, Exception exception);
	}
	
	private ResultParser<Result> resultParser = null;
	private ResultCallback<Result> resultCallback = null;
	private Exception exception = null;
	
	public NattstadWebserviceCall(ResultParser<Result> resultParser, ResultCallback<Result> resultCallback) {
		this.resultParser = resultParser;
		this.resultCallback = resultCallback;
	}
	
	@Override
	protected Result doInBackground(HttpRequestBase... params) {
		try {
			return call(params[0], resultParser);
		}
		catch (Exception e) {
			exception = e;
			e.printStackTrace();
		}
		return null;
	}

	public static <T> T call(HttpRequestBase request, ResultParser<T> resultParser) throws NoSuchAlgorithmException,
																						   KeyManagementException, KeyStoreException,
																						   UnrecoverableKeyException, URISyntaxException, IOException,
																						   ClientProtocolException, Exception {


		HttpClient client = new DefaultHttpClient();

		SSLSocketFactory ssf = SSLSocketFactory.getSocketFactory();
		ssf.setHostnameVerifier(SSLSocketFactory.ALLOW_ALL_HOSTNAME_VERIFIER);
		ClientConnectionManager ccm = client.getConnectionManager();
		SchemeRegistry sr = ccm.getSchemeRegistry();
		sr.register(new Scheme(URL_SCHEME, ssf, 443));
		DefaultHttpClient sslClient = new DefaultHttpClient(ccm,
		        client.getParams());
		
		HttpResponse response = sslClient.execute(request);
		final int statusCode = response.getStatusLine().getStatusCode();
		if (statusCode == 200) {
			InputStream in = response.getEntity().getContent();
			return resultParser.parseResult(in);
		}
		else {
			throw new ErrorCode(statusCode);
		}
	}
	
	@Override
	protected void onPostExecute(Result result) {
		resultCallback.handleResult(result, exception);
	}
	
	public static HttpPost newPostRequest(Context context, Bundle b, Method method) throws ClientProtocolException, IOException {
		SharedPreferences preferences = context.getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, 0);
		String username = preferences.getString(LoginActivity.USERNAME_KEY, "");
		String password = preferences.getString(LoginActivity.PASSWORD_KEY, "");
		Session session = Session.getActiveSession();
		String accessToken = "";
		if (session != null) {
			accessToken = session.getAccessToken();
			if (accessToken == null) accessToken = "";
		}
		
		b.putString("username", username);
		b.putString("password", password);
		b.putString("lang_code", Locale.getDefault().getLanguage());
		b.putString("facebooktoken", accessToken);

		HttpPost post = new HttpPost(NattstadWebserviceCall.URL_SCHEME + "://" + NattstadWebserviceCall.BASE_URL + "/" + method.toString());
		String stringBoundary = "-----------------------------178448449274243042114807987"; 

		post.addHeader("Content-type", "multipart/form-data; boundary=" + stringBoundary);
		post.addHeader("Accept", "*/*");
		post.addHeader("Accept-Language", "en-us");
		post.addHeader("Pragma", "no-cache");
		post.addHeader("Proxy-Connection", "keep-alive");

		String body = "\r\n--" + stringBoundary + "\r\n";

		Set<String> ks = b.keySet();
		Iterator<String> iterator = ks.iterator();
		while (iterator.hasNext()) {
			String key 		= iterator.next();
			Object value	= b.get(key);
			if(value.getClass() == String.class) {
				body += "Content-Disposition: form-data; name=\"" + key + "\"\r\n\r\n";
				body +=  EncodingUtils.getString(((String)value).getBytes(), "ISO-8859-1");
				body += "\r\n--" + stringBoundary + "\r\n";
			}
			else if (value.getClass() == byte[].class) {
				byte[] imageData = (byte[]) value;
				body += "Content-Disposition: form-data; name=\"media\"; filename=\"" + key + "\"\r\n";
				body += "Content-Type: " + "image/jpeg\r\n";
				body += "Content-Transfer-Encoding: " + "binary\r\n\r\n";
				body += EncodingUtils.getString(imageData, "ISO-8859-1");
				body += "\r\n\r\n--" + stringBoundary + "\r\n";
			}
		}

		post.setEntity(new StringEntity(body));
		return post;
	}
	
	public static class ErrorCode extends Exception {
		private int errorCode;
		
		ErrorCode(int errorCode) {
			this.errorCode = errorCode;
		}
		
		int getErrorCode() {
			return errorCode;
		}
	}
}
