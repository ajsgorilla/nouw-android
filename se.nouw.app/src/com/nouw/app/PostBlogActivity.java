package com.nouw.app;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.List;

import org.apache.http.client.methods.HttpGet;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import com.nouw.app.R;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ContentValues;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Gravity;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemSelectedListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.RelativeLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;

import com.aviary.android.feather.library.Constants;
import com.aviary.android.feather.sdk.FeatherActivity;
import com.fourmob.datetimepicker.date.DatePickerDialog;
import com.fourmob.datetimepicker.date.DatePickerDialog.OnDateSetListener;
import com.sleepbot.datetimepicker.time.RadialPickerLayout;
import com.sleepbot.datetimepicker.time.TimePickerDialog;
import com.sleepbot.datetimepicker.time.TimePickerDialog.OnTimeSetListener;

@SuppressLint("UseValueOf")
public class PostBlogActivity extends FragmentActivity implements LocationListener, PopulateCategoriesTaskFragment.TaskCallback {

	private static final String CURRENT_PHOTO_URI_KEY = "currentPhotoUriKey";

	enum ImageSize {
		LARGE,
		MEDIUM,
		SMALL,
		CUSTOM
	};

	private static String[] getImageSizeStrings() {
		String[] ret = NattstadApplication.
				getInstance().
				getResources().
				getStringArray(R.array.size_name);
				
		return ret;
	}

	private static PostBlogActivity instance						= null;

	private static final String		gpsEnabledKey					= "POSTBLOG_GPS_ENABLED";
	private static final String		headerTextKey					= "POSTBLOG_HEADERTEXT_KEY";
	private static final String		descriptionTextKey				= "POSTBLOG_DESCRIPTIONTEXT_KEY";
	private static final String		mediaUrlTextKey					= "POSTBLOG_MEDIAURLTEXT_KEY";
	private static final String		selectedImageKey 				= "SELECTED_IMAGE_KEY";
	private static final String 	blogImagesKey 					= "POSTBLOG_IMAGES_KEY";

	private static final String		defaultPublishDate				= NattstadApplication.getInstance().getResources().getString(R.string.now);
	private static final String[]	imageSizeStrings				= getImageSizeStrings();

	private static final int		ACTIVITY_SELECT_IMAGE 			= 100;
	private static final int		ACTIVITY_TAKE_PHOTO				= 110;
	private static final int 		ACTIVITY_POST_BLOG_PROGRESS		= 120;
	private static final int		ACTIVITY_AVIARY					= 130;

	private static final int 		NUM_BLOG_IMAGES					= 12;
	private static final int		NUM_SELECTABLE_BLOG_CATEGORIES	= 3;

	private Integer 				imageRelativeLayoutClickedId	= null;
	private Uri 					currentPhotoUri 				= null;

	private Uri[] 					blogImagesUri					= new Uri[NUM_BLOG_IMAGES];
	private ImageSize[]				blogImageSizes					= new ImageSize[NUM_BLOG_IMAGES];
	private int[]		 			blogCategoryIds 				= null;
	private int[]					selectedBlogCategoryIds			= null;

	private Spinner 				category0						= null;
	private Spinner 				category1						= null;
	private Spinner 				category2						= null;

	private LocationManager 		locationManager					= null; 
	private Location				location						= null;
	private String					locationProvider				= null;

	private Button 					datePickerButton				= null;			
	private String					publishDate						= defaultPublishDate;
	private String					headerText						= null;
	private String					descriptionText					= null;
	private String 					mediaUrlText 					= null;
	
	private UploadImageSize uploadImageSize = null;
	private PopulateCategoriesTaskFragment populateCategoriesFragment = null;
	private ProgressDialog progressDialog;

	@SuppressLint("UseValueOf")
	private void addClickListenerOnBlogImagePicker(final int idx) {
		String idxString = new Integer(idx).toString();

		RelativeLayout image1RelativeLayout = (RelativeLayout)findViewById(getResources().getIdentifier("image" + idxString + "RelativeLayout" , "id" , getPackageName()));
		image1RelativeLayout.setOnClickListener(new View.OnClickListener(){
			public void onClick(View v) {
				imageRelativeLayoutClickedId = idx;
				registerForContextMenu(v);
				openContextMenu(v);
				unregisterForContextMenu(v);
			}
		});

		Spinner	sizeSpinner	= (Spinner)findViewById(getResources().getIdentifier("imageSize" + idxString + "Spinner" , "id" , getPackageName()));
		ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.simple_list_item, getImageSizeStrings());
		adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		sizeSpinner.setAdapter(adapter);
		
		blogImageSizes[idx] = ImageSize.LARGE;
		sizeSpinner.setOnItemSelectedListener(new OnItemSelectedListener() {    
			public void onItemSelected(AdapterView<?> adapter, 
					View v, 
					int i, 
					long lng) {
				blogImageSizes[idx] = ImageSize.values()[i];
			} 

			public void onNothingSelected(AdapterView<?> parentView) {         
				blogImageSizes[idx] = ImageSize.LARGE;
			}
		}); 
		
		Button editButton = (Button) findViewById(getResources().getIdentifier("editImage" + idxString, "id", getPackageName()));
		editButton.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				if (blogImagesUri[idx] != null) {
					imageRelativeLayoutClickedId = idx;
					Intent newIntent = new Intent(PostBlogActivity.this, FeatherActivity.class);
					newIntent.setData(blogImagesUri[idx]);
					newIntent.putExtra(Constants.EXTRA_IN_API_KEY_SECRET, "9c12a2e9d047a4dd");
					try
					{
						startActivityForResult(newIntent, ACTIVITY_AVIARY);
					}
					catch(Exception ex)
					{
						
					}
				}
			}
		});
	}

	private void resetBlogImage(final int idx) {
		String idxString = new Integer(idx).toString();

		Spinner	sizeSpinner	= (Spinner)findViewById(getResources().getIdentifier("imageSize" + idxString + "Spinner" , "id" , getPackageName()));
		blogImageSizes[idx] = ImageSize.LARGE;
		sizeSpinner.setSelection(0);

		blogImagesUri[idx] = null;
		ImageView imageView = blogImage(idx);
		imageView.setImageResource(R.drawable.picbg);

	}

	private void addHeaderTextListener() {
		EditText header = (EditText)findViewById(R.id.header);
		header.addTextChangedListener(new TextWatcher(){

			public void afterTextChanged(Editable s) {
				headerText = s.toString();
			}
			public void beforeTextChanged(CharSequence s, int start, int count, int after){}
			public void onTextChanged(CharSequence s, int start, int before, int count){}
		}); 
	}

	private void addDescriptionTextListener() {
		MultiAutoCompleteTextView description = (MultiAutoCompleteTextView)findViewById(R.id.description);
		description.setAdapter(new AutoCompleteAdapter(this, R.layout.simple_list_item));
		description.setTokenizer(new NicknameTokenizer());
		
		description.addTextChangedListener(new TextWatcher() {

			public void afterTextChanged(Editable s) {
				descriptionText = s.toString();
			}
			public void beforeTextChanged(CharSequence s, int start, int count, int after){}
			public void onTextChanged(CharSequence s, int start, int before, int count){}
		}); 
	}

	private void addListenerOnGPSCheckBox() {
		CheckBox gpsCheckBox = (CheckBox)findViewById(R.id.usegps);
		gpsCheckBox.setOnClickListener(new OnClickListener() {
			public void onClick(View v) {
				boolean enabled = ((CheckBox)v).isChecked();
				Editor editor = getApplicationContext().getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE).edit();
				editor.putBoolean(gpsEnabledKey, enabled);
				editor.commit();

				enableGPS(enabled);
			}
		});
	}

	private void enableGPS(boolean enabled) {
		if(enabled) {
			locationProvider = locationManager.getBestProvider(new Criteria(), true);
			if (locationProvider != null) {
				locationManager.requestLocationUpdates(locationProvider, 1000, 10f, this);
			}
		} 
		else {
			locationManager.removeUpdates(this);
		}
	}
	
	private void addMediaUrlTextListener() {
		EditText mediaUrl = (EditText) findViewById(R.id.mediaUrl);
		mediaUrl.addTextChangedListener(new TextWatcher(){

			public void afterTextChanged(Editable s) {
				mediaUrlText  = s.toString();
			}
			public void beforeTextChanged(CharSequence s, int start, int count, int after){}
			public void onTextChanged(CharSequence s, int start, int before, int count){}
		}); 
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		instance = this;
		super.onCreate(savedInstanceState);
		setContentView(R.layout.post_blog);
		
		if (savedInstanceState != null) {
			imageRelativeLayoutClickedId = savedInstanceState.getInt(selectedImageKey);
			currentPhotoUri = savedInstanceState.getParcelable(CURRENT_PHOTO_URI_KEY);
			ArrayList<Uri> blogImages = savedInstanceState.getParcelableArrayList(blogImagesKey);
			for (int i = 0; i < blogImages.size(); ++i) {
				blogImagesUri[i] = blogImages.get(i);
				if (blogImagesUri[i] != null) {
					ImageView imageView = blogImage(i);
					try {
						imageView.setImageBitmap(createThumbNail(blogImagesUri[i]));
					}
					catch (IOException e) {
						imageView.setImageResource(R.drawable.picbg);
					}
				}
			}
		}

		category0 = (Spinner) findViewById(R.id.category0);
		category1 = (Spinner) findViewById(R.id.category1);
		category2 = (Spinner) findViewById(R.id.category2);

		for(int i = 0; i < NUM_BLOG_IMAGES; ++i) {
			addClickListenerOnBlogImagePicker(i);
		}

		addListenerOnGPSCheckBox();
		addListenerOnDatePickerButton();
		addListenersOnPublishButtons();
		addDescriptionTextListener();
		addHeaderTextListener();
		addMediaUrlTextListener();

		populateCategories();

		locationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
		locationProvider = locationManager.getBestProvider(new Criteria(), true);
		if (locationProvider != null) {
			location = locationManager.getLastKnownLocation(locationProvider);
		}
		else {
			// ?
		}
		
		try {
			HttpGet get = NattstadWebserviceCall.newGetRequest(this, NattstadWebserviceCall.Method.GetImageUploadWidth, null);

			NattstadWebserviceCall.ResultParser<UploadImageSize> parser = new NattstadWebserviceCall.ResultParser<UploadImageSize>() {
				@Override
				public UploadImageSize parseResult(InputStream in) throws Exception {
					return UploadImageSize.fromXml(in);
				}
			};
			
			NattstadWebserviceCall.ResultCallback<UploadImageSize> callback = new NattstadWebserviceCall.ResultCallback<UploadImageSize>() {
				@Override
				public void handleResult(UploadImageSize result, Exception exception) {
					if (result != null && exception == null) {
						Log.i("PostBlogActivity", "normal size " + Integer.toString(result.normalWidth) + ", slow network size " + Integer.toString(result.slowNetworkWidth));
						uploadImageSize = result;
					}
				}
			};
			
			new NattstadWebserviceCall<UploadImageSize>(parser, callback).execute(get);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View v, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		MenuInflater inflater = getMenuInflater();
		if (blogImagesUri[imageRelativeLayoutClickedId] == null) {
			inflater.inflate(R.menu.post_blog_take_or_choose_picture, menu);
		}
		else {
			inflater.inflate(R.menu.post_blog_take_or_choose_picture_or_delete, menu);
		}
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_take_photo:
			Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
		    if (takePictureIntent.resolveActivity(getPackageManager()) != null) {
		        	currentPhotoUri = getContentResolver().insert(MediaStore.Images.Media.EXTERNAL_CONTENT_URI, new ContentValues());
		            takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, currentPhotoUri);
		            startActivityForResult(takePictureIntent, ACTIVITY_TAKE_PHOTO);
		    }

			return true;
		case R.id.action_choose_photo:
			Intent intent = new Intent(Intent.ACTION_PICK,
					android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
			startActivityForResult(intent, ACTIVITY_SELECT_IMAGE);
			return true;
		case R.id.action_remove_photo:
			removePhotoAndRearrange(imageRelativeLayoutClickedId);
			return true;
		default:
			return super.onContextItemSelected(item);
		}
	}

	private void removePhotoAndRearrange(Integer idx) {
		if (idx != null) {
			removePhoto(idx);
			
			for (int i = idx; i < blogImagesUri.length; ++i) {
				if (i + 1 < blogImagesUri.length && blogImagesUri[i + 1] != null) {
					copyPhoto(i, i + 1);
					removePhoto(i + 1);
				}
			}
		}
	}

	private void copyPhoto(int to, int from) {
		blogImagesUri[to] = blogImagesUri[from];
		blogImageSizes[to] = blogImageSizes[from];
		ImageView imageView = blogImage(to);
		try {
			imageView.setImageBitmap(createThumbNail(blogImagesUri[to]));
		}
		catch (FileNotFoundException e) {
			blogImagesUri[to] = null;
			blogImageSizes[to] = ImageSize.LARGE;
			imageView.setImageResource(R.drawable.picbg);
		}
	}

	private void removePhoto(Integer idx) {
		blogImagesUri[idx] = null;
		blogImageSizes[idx] = ImageSize.LARGE;
		ImageView imageView = blogImage(idx);
		imageView.setImageResource(R.drawable.picbg);
	}
	
	private int indexOfFirstEmptyImage() {
		for (int i = 0; i < blogImagesUri.length; ++i) {
			if (blogImagesUri[i] == null) return i;
		}
		return -1;
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.layout.post_blog_menu, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.empty:     
			AlertDialog.Builder builder = new AlertDialog.Builder(this);
			builder.setMessage(R.string.empty_are_you_sure)
			.setCancelable(false)
			.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					empty();
				}
			})
			.setNegativeButton("No", new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int id) {
					dialog.cancel();
				}
			});
			AlertDialog alert = builder.show();
			TextView messageView = (TextView)alert.findViewById(android.R.id.message);
			messageView.setGravity(Gravity.CENTER);
			break;
		}
		return true;
	}

	private void resetSharedPreferences() {
		SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
		if(sharedPreferences.contains(headerTextKey)) {
			Editor editor = sharedPreferences.edit();
			editor.remove(headerTextKey);
			editor.commit();
		}

		if(sharedPreferences.contains(descriptionTextKey)) {
			Editor editor = sharedPreferences.edit();
			editor.remove(descriptionTextKey);
			editor.commit();
		}
		
		if (sharedPreferences.contains(mediaUrlTextKey)) {
			Editor editor = sharedPreferences.edit();
			editor.remove(mediaUrlTextKey);
			editor.commit();
		}
		
		EditText header = (EditText)findViewById(R.id.header);
		header.setText("");
		EditText description = (EditText)findViewById(R.id.description);
		description.setText("");
		EditText mediaUrl = (EditText) findViewById(R.id.mediaUrl);
		mediaUrl.setText("");
	}

	private void empty() {
		resetSharedPreferences();

		for(int i = 0; i < NUM_BLOG_IMAGES; ++i) {
			resetBlogImage(i);
		}

		category0.setSelection(0);
		category1.setSelection(0);
		category2.setSelection(0);

		publishDate = defaultPublishDate;
		final Spinner publishDateSpinner = (Spinner) findViewById(R.id.choose_date_time);
		publishDateSpinner.setAdapter(new ArrayAdapter<String>(PostBlogActivity.this, R.layout.simple_list_item, new String[]{publishDate}));
	}

	private Bundle postBlogBodyBundle(boolean asDraft, boolean hasPictures) {
		Bundle ret = new Bundle();
		ret.putString("draft", asDraft ? "true" : "false");
		
		boolean gpsEnabled = getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, 0).getBoolean(gpsEnabledKey, false);
		if (location != null && gpsEnabled) {
			ret.putString("longitude", Double.toString(location.getLongitude()));
			ret.putString("latitude", Double.toString(location.getLatitude()));
		} else {
			ret.putString("longitude", "0");
			ret.putString("latitude", "0");
		}
		ret.putString("header", TextUtils.htmlEncode(headerText));
		ret.putString("description", descriptionText != null ? TextUtils.htmlEncode(descriptionText) : "");
		ret.putString("link_url", mediaUrlText != null ? TextUtils.htmlEncode(mediaUrlText) : "");

		ret.putString("no_pictures", hasPictures ? "false" : "true");
		ret.putString("publishdate", (publishDate.compareTo(defaultPublishDate) == 0) ? "" : publishDate);

		for(int i = 0; i < 3; ++i) {
			int blogCategoryId = blogCategoryIds != null ? blogCategoryIds[selectedBlogCategoryIds[i]] : 0;
			ret.putString("blogcategory" + Integer.toString(i + 1), Integer.toString(blogCategoryId));
		}
		return ret;
	}

	private Bundle postPictureBodyBundle() {
		Bundle ret = new Bundle();
		int numImages = 0;
		for(int i = 0; i < NUM_BLOG_IMAGES; ++i) {
			Bundle image = new Bundle();
			if(blogImagesUri[i] != null) {
				image.putParcelable("uri", 	blogImagesUri[i]);
				image.putString("size", imageSizeStrings[blogImageSizes[i].ordinal()]);
				ret.putBundle("image" + Integer.toString(i), image);
				++numImages;
			}
		}
		if(ret.size() > 0) {
			ret.putInt("numImages", numImages);
			return ret;
		} else {
			return null;
		}
	}

	private void publish(boolean asDraft) {
		if(headerText == null || headerText.length() == 0) {
			return;
		}
		//Log.d("publish", new Boolean(asDraft).toString());
		Intent intent = new Intent(PostBlogActivity.this, PostBlogProgressActivity.class);

		Bundle b = new Bundle();
		Bundle postPictureBody = postPictureBodyBundle();

		b.putBundle("postBlogBody", postBlogBodyBundle(asDraft, postPictureBody != null));

		if(postPictureBody != null) {
			b.putBundle("postPictureBody", postPictureBody);
		}
		
		if (uploadImageSize != null) {
			b.putInt("normalWidth", uploadImageSize.normalWidth);
			b.putInt("slowNetworkWidth", uploadImageSize.slowNetworkWidth);
		}

		intent.putExtras(b);

		startActivityForResult(intent, ACTIVITY_POST_BLOG_PROGRESS);
	}

	private void addListenersOnPublishButtons() {
		Typeface typeface = Typeface.createFromAsset(getAssets(), "fonts/Roboto-Light.ttf");
		Button draftButton = (Button) findViewById(R.id.draftButton);
		draftButton.setTypeface(typeface);
		draftButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				publish(true);
			}
		});

		Button publishButton = (Button) findViewById(R.id.publishButton);
		publishButton.setTypeface(typeface);
		publishButton.setOnClickListener(new OnClickListener() {
			public void onClick(View arg0) {
				publish(false);
			}
		});
	}

	private void addListenerOnDatePickerButton() {
		final Spinner publishDateSpinner = (Spinner) findViewById(R.id.choose_date_time);
		publishDateSpinner.setAdapter(new ArrayAdapter<String>(PostBlogActivity.this, R.layout.simple_list_item, new String[]{publishDate}));
		publishDateSpinner.setOnTouchListener(new View.OnTouchListener() {
			@Override
			public boolean onTouch(View v, MotionEvent event) {
				if (event.getAction() == MotionEvent.ACTION_UP) {
					final Calendar calendar = Calendar.getInstance();
					DatePickerDialog.newInstance(new OnDateSetListener() {
						@Override
						public void onDateSet(DatePickerDialog datePickerDialog, final int year, final int month, final int day) {
							TimePickerDialog.newInstance(new OnTimeSetListener() {
								@Override
								public void onTimeSet(RadialPickerLayout view, int hourOfDay, int minute) {								
									publishDate = String.format("%d-%02d-%02d %02d:%02d", year, month + 1, day, hourOfDay, minute);
									publishDateSpinner.setAdapter(new ArrayAdapter<String>(PostBlogActivity.this, R.layout.simple_list_item, new String[]{publishDate}));
								}
							}, calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.MINUTE), true, false).show(getSupportFragmentManager(), "time_picker");
						}
					}, calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH), calendar.get(Calendar.DAY_OF_MONTH), false).show(getSupportFragmentManager(), "date_picker");
					return true;
				}
				return false;
			}
		});
	}

	@Override
	protected void onStart() {
		super.onStart();
		// The activity is about to become visible.
	}

	@Override
	protected void onResume() {
		super.onResume();
		
		for (int i = 0; i < blogImagesUri.length; ++i) {
			if (blogImagesUri[i] != null) {
				ImageView imageView = blogImage(i);
				try {
					imageView.setImageBitmap(createThumbNail(blogImagesUri[i]));
				}
				catch (IOException e) {
					imageView.setImageResource(R.drawable.picbg);
				}
			}
		}

		SharedPreferences sharedPreferences = getApplicationContext().getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE);
		if(!sharedPreferences.contains(gpsEnabledKey)) {
			Editor editor = sharedPreferences.edit();
			editor.putBoolean(gpsEnabledKey, true);
			editor.commit();
		}

		if(sharedPreferences.contains(headerTextKey)) {
			EditText header 		= (EditText)findViewById(R.id.header);
			header.setText(sharedPreferences.getString(headerTextKey, ""));
		}

		if(sharedPreferences.contains(descriptionTextKey)) {
			EditText description 	= (EditText)findViewById(R.id.description);
			description.setText(sharedPreferences.getString(descriptionTextKey, ""));
		}
		
		if(sharedPreferences.contains(mediaUrlTextKey)) {
			EditText mediaUrl 	= (EditText)findViewById(R.id.mediaUrl);
			mediaUrl.setText(sharedPreferences.getString(mediaUrlTextKey, ""));
		}

		final boolean gpsEnabled = sharedPreferences.getBoolean(gpsEnabledKey, false);

		CheckBox gpsCheckBox = (CheckBox)findViewById(R.id.usegps);
		gpsCheckBox.setChecked(gpsEnabled);

		enableGPS(gpsEnabled);
	}
	
	@Override
	public void onWindowFocusChanged(boolean hasFocus) {
		super.onWindowFocusChanged(hasFocus);

		SharedPreferences sharedPreferences = getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, 0);
		if (hasFocus) {
			final ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView1);
			final int scrollY = sharedPreferences.getInt("scrollY", 0);
			scrollView.post(new Runnable() {
				@Override
				public void run() {
					scrollView.scrollTo(0, scrollY);
				}
			});
		}
		else {
			ScrollView scrollView = (ScrollView) findViewById(R.id.scrollView1);
			int scrollY = scrollView.getScrollY();
			sharedPreferences.edit().putInt("scrollY", scrollY).commit();
		}
	}

	@Override
	protected void onPause() {
		super.onPause();
		enableGPS(false);
		Editor editor = getApplicationContext().getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, Context.MODE_PRIVATE).edit();
		if(headerText != null) {
			editor.putString(headerTextKey, headerText);
		}

		if(descriptionText != null) {
			editor.putString(descriptionTextKey, descriptionText);
		}
		
		if (mediaUrlText != null) {
			editor.putString(mediaUrlTextKey, mediaUrlText);
		}
		
		editor.commit();
	}

	@Override
	protected void onStop() {
		super.onStop();
		// The activity is no longer visible (it is now "stopped")
	}

	@Override
	protected void onDestroy() {
		super.onDestroy();
		// The activity is about to be destroyed.
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if (imageRelativeLayoutClickedId != null) {
			outState.putInt(selectedImageKey, imageRelativeLayoutClickedId);
		}
		
		if (currentPhotoUri != null) {
			outState.putParcelable(CURRENT_PHOTO_URI_KEY, currentPhotoUri);
		}
		
		ArrayList<Uri> blogImages = new ArrayList<Uri>(Arrays.asList(blogImagesUri));
		outState.putParcelableArrayList(blogImagesKey, blogImages);
	}
	
	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
	}

	@SuppressLint("UseValueOf")
	ImageView blogImage(final Integer idx) {
		String idxString = new Integer(idx).toString();
		return (ImageView)findViewById(getResources().getIdentifier("image" + idxString, "id" , getPackageName()));
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent imageReturnedIntent) { 
		super.onActivityResult(requestCode, resultCode, imageReturnedIntent); 

		switch(requestCode) { 
		case ACTIVITY_TAKE_PHOTO:
			if (resultCode == RESULT_OK) {
				blogImagesUri[imageRelativeLayoutClickedId] = currentPhotoUri;
				try {
					ImageView imageView = blogImage(imageRelativeLayoutClickedId);
					Bitmap thumbnail 	= createThumbNail(blogImagesUri[imageRelativeLayoutClickedId]);
					imageView.setImageBitmap(thumbnail);
					int firstEmpty = indexOfFirstEmptyImage();
					if (firstEmpty < imageRelativeLayoutClickedId && firstEmpty >= 0) {
						copyPhoto(firstEmpty, imageRelativeLayoutClickedId);
						removePhoto(imageRelativeLayoutClickedId);
					}
				}
				catch (Exception e) {
					blogImagesUri[imageRelativeLayoutClickedId] = null;
				}
			}
			break;
		case ACTIVITY_SELECT_IMAGE:
			if(resultCode == RESULT_OK){ 
				blogImagesUri[imageRelativeLayoutClickedId] = imageReturnedIntent.getData();
				try {
					ImageView imageView = blogImage(imageRelativeLayoutClickedId);
					Bitmap thumbnail 	= createThumbNail(blogImagesUri[imageRelativeLayoutClickedId]);
					imageView.setImageBitmap(thumbnail);
					int firstEmpty = indexOfFirstEmptyImage();
					if (firstEmpty < imageRelativeLayoutClickedId && firstEmpty >= 0) {
						copyPhoto(firstEmpty, imageRelativeLayoutClickedId);
						removePhoto(imageRelativeLayoutClickedId);
					}
				} catch (Exception e) {
					blogImagesUri[imageRelativeLayoutClickedId] = null;
					e.printStackTrace();
				}
			}
			break;
		case ACTIVITY_POST_BLOG_PROGRESS:
			if(resultCode == RESULT_OK) {
				resetSharedPreferences();
				Intent intent = getIntent();
				setResult(RESULT_OK, intent);
				finish();
			}
			break;
		case ACTIVITY_AVIARY:
			if (resultCode == RESULT_OK) {
				Uri editedImageUri = imageReturnedIntent.getData();
				blogImagesUri[imageRelativeLayoutClickedId] = editedImageUri;
				Bundle extra = imageReturnedIntent.getExtras();
                if (null != extra) {
                    boolean changed = extra.getBoolean(Constants.EXTRA_OUT_BITMAP_CHANGED);
                    if (changed) {
                    	try {
        					ImageView imageView = blogImage(imageRelativeLayoutClickedId);
        					Bitmap thumbnail 	= createThumbNailMile(blogImagesUri[imageRelativeLayoutClickedId]);
        					imageView.setImageBitmap(thumbnail);
        				} catch (Exception e) {
        					blogImagesUri[imageRelativeLayoutClickedId] = null;
        					e.printStackTrace();
        				}
                    }
                }
			}
			break;
		}
	}
	
	private Bitmap createThumbNailMile(Uri selectedImage) throws FileNotFoundException {
		try {
			int orientation = Utility.getOrientationOfImage(this, selectedImage);
			int degrees = 0;
			if (orientation == ExifInterface.ORIENTATION_ROTATE_90) degrees = 90;
			if (orientation == ExifInterface.ORIENTATION_ROTATE_270) degrees = 270;
			if (orientation == ExifInterface.ORIENTATION_ROTATE_180) degrees = 180;
			Bitmap scaled = Utility.scaledBitmap(this, selectedImage, 140, 140);
			return Utility.rotatedBitmap(this, scaled, degrees);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	private Bitmap createThumbNail(Uri selectedImage) throws FileNotFoundException {
		try {
			int orientation = Utility.getOrientationOfImage(this, selectedImage);
			int degrees = 0;
			if (orientation == ExifInterface.ORIENTATION_ROTATE_90) degrees = 90;
			if (orientation == ExifInterface.ORIENTATION_ROTATE_270) degrees = 270;
			if (orientation == ExifInterface.ORIENTATION_ROTATE_180) degrees = 180;
			Bitmap scaled = Utility.scaledBitmap(this, selectedImage, 140, 140);
			return Utility.rotatedBitmap(this, scaled, degrees);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void populateCategories() {
		FragmentManager fm = getSupportFragmentManager();
		populateCategoriesFragment = (PopulateCategoriesTaskFragment) fm.findFragmentByTag("populateCategoriesFragment");
		if (populateCategoriesFragment == null) {
			populateCategoriesFragment = new PopulateCategoriesTaskFragment();
			FragmentTransaction transaction = fm.beginTransaction();
			transaction.add(populateCategoriesFragment, "populateCategoriesFragment");
			transaction.commit();
		}
	}
	public void onLocationChanged(Location l) {
		location = l;
		//TextView t = (TextView)findViewById(R.id.TextView01);
		//t.setText(l.toString());
		Log.d("GPS", "Location Changed");
	}

	public void onProviderDisabled(String provider) {
		Log.d("GPS", "Disabled");

	}

	public void onProviderEnabled(String provider) {
		Log.d("GPS", "Enabled");
	}

	public void onStatusChanged(String provider, int status, Bundle extras) {
		Log.d("GPS", "Status Changed: " + new Integer(status).toString());
	}
	
	public void onPreExecute() {
		if (progressDialog == null) {
			progressDialog = ProgressDialog.show(this, "", PostBlogActivity.this.getString(R.string.loading), true);
		}
	}
	
	@Override
	public void onPostExecute(NodeList nl) {
		if (nl != null) {
			List<String> spinnerStrings = new ArrayList<String>();
			blogCategoryIds = new int[nl.getLength()];
			for (int i = 0; i < nl.getLength(); i++) {
				Element e = (Element) nl.item(i);
				int 	id 		= Integer.parseInt(XMLParser.getValue(e, "CategoryID")); // name child value
				String 	name 	= XMLParser.getValue(e, "CategoryName"); // cost child value
				spinnerStrings.add(name);
				blogCategoryIds[i] = id;
			}
			selectedBlogCategoryIds = new int[NUM_SELECTABLE_BLOG_CATEGORIES];
			for(int i = 0; i < NUM_SELECTABLE_BLOG_CATEGORIES; ++i) {
				selectedBlogCategoryIds[i] = 0;
			}

			ArrayAdapter<String> spinnerArrayAdapter = new ArrayAdapter<String>(PostBlogActivity.instance, R.layout.simple_list_item, spinnerStrings);
			spinnerArrayAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

			category0.setAdapter(spinnerArrayAdapter);
			category0.setOnItemSelectedListener(new OnItemSelectedListener() {    
				public void onItemSelected(AdapterView<?> adapter, 
						View v, 
						int i, 
						long lng) {
					selectedBlogCategoryIds[0] = i;
				} 

				public void onNothingSelected(AdapterView<?> parentView) {         
					selectedBlogCategoryIds[0] = 0;
				}
			}); 
			category1.setAdapter(spinnerArrayAdapter);
			category1.setOnItemSelectedListener(new OnItemSelectedListener() {    
				public void onItemSelected(AdapterView<?> adapter, 
						View v, 
						int i, 
						long lng) {
					selectedBlogCategoryIds[1] = i;
				} 

				public void onNothingSelected(AdapterView<?> parentView) {         
					selectedBlogCategoryIds[1] = 0;
				}
			}); 
			category2.setAdapter(spinnerArrayAdapter);
			category2.setOnItemSelectedListener(new OnItemSelectedListener() {    
				public void onItemSelected(AdapterView<?> adapter, 
						View v, 
						int i, 
						long lng) {
					selectedBlogCategoryIds[2] = i;
				}

				public void onNothingSelected(AdapterView<?> parentView) {         
					selectedBlogCategoryIds[2] = 0;
				}
			});
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			populateCategoriesFragment = null;
		}
		else {
			// retry
			if (progressDialog != null) {
				progressDialog.dismiss();
				progressDialog = null;
			}
			populateCategoriesFragment = null;
			populateCategories();
		}
	}
}
