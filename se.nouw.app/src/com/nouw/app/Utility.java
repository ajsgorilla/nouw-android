package com.nouw.app;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import com.nouw.app.R;


import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.CursorLoader;
import android.content.DialogInterface;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Gravity;
import android.widget.TextView;


public class Utility {
	public static int getOrientationOfImage(Context context, Uri photoUri) {
		try {
			String[] projection = { MediaStore.Images.Media.DATA }; 
            CursorLoader loader = new CursorLoader(context, photoUri, projection, null, null, null);
            Cursor cursor = loader.loadInBackground();

            int column_index_data = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            cursor.moveToFirst();

            // Rotation is stored in an EXIF tag, and this tag seems to return 0 for URIs.
            // Hence, we retrieve it using an absolute path instead!
            String realPath = cursor.getString(column_index_data);
			ExifInterface exif = new ExifInterface(realPath);
			return exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_NORMAL);
		} 
		catch (Exception e) {
			Log.i("Utility", "getOrientationOfImage failed to get exif data");
		}
		
		return ExifInterface.ORIENTATION_NORMAL;
	}
	

	public static Bitmap scaledBitmap(Context context, Uri photoUri, int maxWidth, int maxHeight) throws IOException {
		
		if(!photoUri.getPath().contains("aviary-image"))
		{
			InputStream is = context.getContentResolver().openInputStream(photoUri);
			BitmapFactory.Options dbo = new BitmapFactory.Options();
			dbo.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(is, null, dbo);
			is.close();

			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPurgeable = true;
			options.inInputShareable = true;
			
			Log.i("Utility", String.format("max heap size %d, free size %d, total memory %d", Runtime.getRuntime().maxMemory(), Runtime.getRuntime().freeMemory(), Runtime.getRuntime().totalMemory()));
			
			Bitmap srcBitmap = null;
			is = context.getContentResolver().openInputStream(photoUri);
			if (dbo.outWidth > maxWidth || dbo.outHeight > maxHeight) {
				float widthRatio = ((float) dbo.outWidth) / ((float) maxWidth);
				float heightRatio = ((float) dbo.outHeight) / ((float) maxHeight);
				float maxRatio = Math.max(widthRatio, heightRatio);

				Log.i("Utility", String.format("width %d, height %d", dbo.outWidth, dbo.outHeight));
				Log.i("Utility", String.format("max width %d, max height %d, in sample size %d", maxWidth, maxHeight, (int)maxRatio));
				options.inSampleSize = (int) maxRatio;
				srcBitmap = BitmapFactory.decodeStream(is, null, options);
			} 
			else {
				Log.i("Utility", String.format("width %d, height %d", dbo.outWidth, dbo.outHeight));
				srcBitmap = BitmapFactory.decodeStream(is, null, options);
			}
			is.close();

			return srcBitmap;
		}
		else
		{
			//InputStream is = context.getContentResolver().openInputStream(photoUri);
			File file = new File(photoUri.toString());
			FileInputStream is = new FileInputStream(file);
			
			BitmapFactory.Options dbo = new BitmapFactory.Options();
			dbo.inJustDecodeBounds = true;
			BitmapFactory.decodeStream(is, null, dbo);
			is.close();

			BitmapFactory.Options options = new BitmapFactory.Options();
			options.inPurgeable = true;
			options.inInputShareable = true;
			
			Log.i("Utility", String.format("max heap size %d, free size %d, total memory %d", Runtime.getRuntime().maxMemory(), Runtime.getRuntime().freeMemory(), Runtime.getRuntime().totalMemory()));
			
			Bitmap srcBitmap = null;
			is = new FileInputStream(file);
//			is = context.getContentResolver().openInputStream(photoUri);
			if (dbo.outWidth > maxWidth || dbo.outHeight > maxHeight) {
				float widthRatio = ((float) dbo.outWidth) / ((float) maxWidth);
				float heightRatio = ((float) dbo.outHeight) / ((float) maxHeight);
				float maxRatio = Math.max(widthRatio, heightRatio);

				Log.i("Utility", String.format("width %d, height %d", dbo.outWidth, dbo.outHeight));
				Log.i("Utility", String.format("max width %d, max height %d, in sample size %d", maxWidth, maxHeight, (int)maxRatio));
				options.inSampleSize = (int) maxRatio;
				srcBitmap = BitmapFactory.decodeStream(is, null, options);
			} 
			else {
				Log.i("Utility", String.format("width %d, height %d", dbo.outWidth, dbo.outHeight));
				srcBitmap = BitmapFactory.decodeStream(is, null, options);
			}
			is.close();

			return srcBitmap;
		}
		
	}
	
	public static Bitmap rotatedBitmap(Context context, Bitmap original, int degrees) {
		Matrix matrix = new Matrix();
		matrix.postRotate(degrees);
		return Bitmap.createBitmap(original, 0, 0, original.getWidth(), original.getHeight(), matrix, true);
	}

	public static byte[] createJPEG(Context context, Uri image, int width, int height) throws IOException {
		Bitmap ret = Utility.scaledBitmap(context, image, width, height);
		ByteArrayOutputStream jpegBytes = new ByteArrayOutputStream();
		ret.compress(Bitmap.CompressFormat.JPEG, 80, jpegBytes);
		byte[] byteArray = jpegBytes.toByteArray();
		ret.recycle();
		return byteArray;
	}

	@SuppressWarnings("deprecation")
	public static void showRetryDialog(Context context, 
			String message, 
			DialogInterface.OnClickListener clickListener, 
			Dialog.OnCancelListener cancelListener) {
		AlertDialog alertDialog = new AlertDialog.Builder(context).create();
		alertDialog.setTitle("Nattstad");
		alertDialog.setMessage(message);
		alertDialog.setButton(context.getString(R.string.try_again), clickListener); 
		alertDialog.setOnCancelListener(cancelListener);
		alertDialog.setIcon(R.drawable.ic_launcher);
		alertDialog.show();
		TextView messageView = (TextView)alertDialog.findViewById(android.R.id.message);
		if (messageView != null) {
			messageView.setGravity(Gravity.CENTER);
		}
	}
}

