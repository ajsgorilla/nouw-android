package com.nouw.app;


import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.net.http.SslError;
import android.os.Bundle;
import android.os.Message;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.HttpAuthHandler;
import android.webkit.JavascriptInterface;
import android.webkit.JsResult;
import android.webkit.SslErrorHandler;
import android.webkit.WebChromeClient;
import android.webkit.WebView;
import android.webkit.WebView.HitTestResult;
import android.webkit.WebViewClient;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EncodingUtils;

import com.facebook.Session;
import com.handmark.pulltorefresh.library.PullToRefreshBase;
import com.handmark.pulltorefresh.library.PullToRefreshBase.OnRefreshListener;
import com.handmark.pulltorefresh.library.PullToRefreshWebView;
import com.nouw.app.NattstadWebserviceCall.ResultCallback;
import com.nouw.app.SideMenuItem.MenuOption;

import com.nouw.app.R;

/**
 * Created by Erik on 2013-12-12.
 */
public class WebViewFragment extends Fragment {
	public static final String WEBVIEW_BASE_URL = "https://m.nouw.com/";
//    public static final String WEBVIEW_BASE_URL = "https://m2.nattstad.se/";
//	public static final String WEBVIEW_BASE_URL = "http://devn.nattstad.se/";
	private MenuOption menuOption = null;
    private String startUrl = null;
    private static final String MENU_OPTION_KEY = "se.nattstad.menu_option_key";
    private static final String START_URL_KEY = "se.nattstad.start_url";
    private WebView webView = null;
    private String currentUrl;
    private String title = null;
    private Integer blogUserId = null;
    private Boolean isFollowingBlog = null;
    
    public static WebViewFragment newInstance(MenuOption option, String startUrl) {
    	WebViewFragment fragment = new WebViewFragment();
    	Bundle args = new Bundle(1);
    	args.putSerializable(MENU_OPTION_KEY, option);
    	args.putString(START_URL_KEY, startUrl);
    	fragment.setArguments(args);
    	return fragment;
    }
    
    @Override
    public void onCreate(Bundle savedInstanceState) {
    	super.onCreate(savedInstanceState);
    	this.menuOption = (MenuOption) getArguments().getSerializable(MENU_OPTION_KEY);
    	this.startUrl = getArguments().getString(START_URL_KEY);
    	SideMenuItem item = new SideMenuItem(0);
    	this.title = item.menuOptionToString(menuOption);
    	Log.i("WebViewFragment", "onCreate [" + menuOption.toString() + "]");
    }
    
    public void didLogin() {
    	loadUrl(createUrl(menuOption));
    }
    
    public void updatePage() {
    	if (webView != null) {
    		Log.i("WebViewFragment", "updatePage called [" + webView.getUrl() + "]");
    		webView.loadUrl("javascript:(function(){ var exists = typeof updatePage == \"function\"; alert(exists ? updatePage() : false) })()");
    	}
    }
    
    public boolean goBackIfPossible() {
    	if (webView != null && webView.canGoBack()) {
    		webView.goBack();
    		return true;
    	}
    	else {
    		return false;
    	}
    }

    @SuppressLint("SetJavaScriptEnabled")
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.webview_fragment, container, false);
        PullToRefreshWebView pullToRefreshWebView = (PullToRefreshWebView) rootView.findViewById(R.id.webView);
        pullToRefreshWebView.setOnRefreshListener(new OnRefreshListener<WebView>() {
			@Override
			public void onRefresh(PullToRefreshBase<WebView> refreshView) {
				updatePage();
				refreshView.onRefreshComplete();
			}
		});
        webView = pullToRefreshWebView.getRefreshableView();
        webView.setWebViewClient(new CustomWebViewClient());
        webView.setWebChromeClient(new WebChromeClient() {
        	@Override
        	public boolean onJsAlert(WebView view, String url, String message, JsResult result) {
        		Log.i("WebChromeClient", "updatePage returned " + message);
        		if (!message.equals("true")) {
        			Log.i("WebChromeClient", "reloading webview");
        			webView.reload();
        		}
        		result.confirm();
        		return true;
        	}
        });
        webView.addJavascriptInterface(new NattstadJavascriptInterface(), "Android");
        webView.getSettings().setJavaScriptEnabled(true);
        webView.getSettings().setDomStorageEnabled(true);
        webView.getSettings().setLoadWithOverviewMode(true);
        webView.getSettings().setUseWideViewPort(true);
        
        loadUrl(createUrl(menuOption));

        return rootView;
    }

	public String createUrl(MenuOption option) {
		if (option != MenuOption.None) {
			String urlPart = SideMenuItem.menuOptionToUrl(option);
			if (urlPart.contains("%d")) {
				urlPart = String.format(urlPart, 0);
			}
			if (urlPart.contains("%s")) {
				SharedPreferences prefs = getActivity().getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, 0);
				urlPart = String.format(urlPart, prefs.getString(LoginActivity.NICKNAME_KEY, ""));
			}

			String url = WEBVIEW_BASE_URL + urlPart;
			Uri.Builder builder = Uri.parse(url).buildUpon();
			if (!url.contains("app=true")) {
				builder.appendQueryParameter("app", "true");
			}
			
			if(!url.contains("lang_code")){
				builder.appendQueryParameter("lang_code", Locale.getDefault().getLanguage());
			}
			
			Log.i("WebViewFragment", "createUrl [" + builder.build().toString() + "]");
			return builder.build().toString();
		}
		else {
			if (startUrl.contains("nsopenurl")) {
				startUrl = startUrl.replace("nsopenurl", "http");
			}
			Log.i("WebViewFragment", "createUrl [" + startUrl + "]");
			return startUrl;
		}
	}
    
    private void loadUrl(String url) {
    	Log.i("WebViewFragment", "loadUrl [" + url + "]");
    	if (url.contains("nouw.com")) {
    		SharedPreferences preferences = getActivity().getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, 0);
    		String parameters = "";
    		String username = preferences.getString(LoginActivity.USERNAME_KEY, "");
    		String password = preferences.getString(LoginActivity.PASSWORD_KEY, "");
    		Session session = Session.getActiveSession();
    		String accessToken = "";
    		if (session != null) {
    			accessToken = session.getAccessToken();
    			if (accessToken == null) accessToken = "";
    		}
    		
    		if ((!username.isEmpty() && !password.isEmpty()) || !accessToken.isEmpty()) {
    			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
    			params.add(new BasicNameValuePair("username", username));
    			params.add(new BasicNameValuePair("password", password));
    			params.add(new BasicNameValuePair("facebooktoken", accessToken));
    			parameters = URLEncodedUtils.format(params, "utf-8");

    			Uri.Builder builder = Uri.parse(url).buildUpon();
    			if (!url.contains("app=true")) {
    				builder.appendQueryParameter("app", "true");
    			}
    			
    			if(!url.contains("lang_code")){
    				builder.appendQueryParameter("lang_code", Locale.getDefault().getLanguage());
    			}
    			
    			String titleString = Uri.parse(url).getQueryParameter("nstitle");
    			if (titleString != null) {
    				this.title = titleString; 
    				MainActivity activity = (MainActivity) getActivity();
    				activity.restoreActionBar();
    			}
    			
    			String userIdString = Uri.parse(url).getQueryParameter("nsuserid");
    			if (userIdString != null) {
    				blogUserId = Integer.parseInt(userIdString);
    				checkFollowStatus(userIdString);
    			}

    			String appUrl = builder.build().toString();
    			currentUrl = appUrl;
    			webView.postUrl(appUrl, EncodingUtils.getBytes(parameters, "utf-8"));
    		}
    		else {
    			currentUrl = "about:blank";
    			webView.loadUrl(currentUrl);
    		}
    	}
    	else {
    		currentUrl = url;
    		webView.loadUrl(url);
    	}
    }

	private void checkFollowStatus(String userId) {
		try {
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("memberid", userId));
			//BasicNameValuePair pair = new BasicNameValuePair("lang_code",Locale.getDefault().getLanguage());
			//params.add(pair);
			HttpGet get = NattstadWebserviceCall.newGetRequest(getActivity(), NattstadWebserviceCall.Method.GetFollow, params);
			new NattstadWebserviceCall<Boolean>(new BasicParsers.BoolParser(), new ResultCallback<Boolean>() {
				@Override
				public void handleResult(Boolean isFollowing, Exception exception) {
					if (isFollowing != null && exception == null) {
						Log.i("WebViewFragment", "is following blog? " + isFollowing.toString());
						isFollowingBlog = isFollowing;
						MainActivity activity = (MainActivity) getActivity();
						if (activity != null) {
							activity.updateBlogMenu(isFollowing);
						}
					}
					else if (exception != null) {
						Log.i("WebViewFragment", "exception: " + exception.getMessage());
					}
				}
			}).execute(get);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}
    
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        ((MainActivity) activity).onSectionAttached(0);
    }

    private class CustomWebViewClient extends WebViewClient {
        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            Log.i("CustomWebViewClient", "should override url loading " + url);
            
            if (url.contains("nssamepage=true")) {
            	currentUrl = url;
            	startUrl = url;
            	loadUrl(createUrl(MenuOption.None));
            	return true;
            }
            
            HitTestResult hitTestResult = view.getHitTestResult();
			if (hitTestResult == null || hitTestResult.getType() == HitTestResult.UNKNOWN_TYPE) { 
				// UNKNOWN_TYPE probably means a redirect, but no way to be sure because android....
            	Log.i("CustomWebViewClient", "should be redirect in fragment " + getTag());
            	return false;
            }
            else if (url.contains("nouw.com") || (url.contains("nsopenurl"))) {
            	MainActivity activity = (MainActivity) getActivity();
            	activity.addNewWebViewFragment(url);
            	return true;
            }
            else {
            	Intent externalBrowser = new Intent(Intent.ACTION_VIEW, Uri.parse(url.replace("https", "http")));
            	startActivity(externalBrowser);
            	return true;
            }
        }
        
        @Override
        public void onPageStarted(WebView view, String url, Bitmap favicon) {
        	super.onPageStarted(view, url, favicon);
        	Log.i("CustomWebViewClient", "page started loading " + url);
        }
        
        @Override
        public void onPageFinished(WebView view, String url) {
        	super.onPageFinished(view, url);
        	if (url.contains("nssamepage=true")) {
				webView.clearHistory();
			}
        	Log.i("CustomWebViewClient", "finished loading " + url);
        }
        
        @Override
        public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
//        	super.onReceivedError(view, errorCode, description, failingUrl);
        	Log.i("WebViewClient", "error " + Integer.toString(errorCode) + ": " + description + " [url " + failingUrl + "]"); 
        	loadUrl(failingUrl);
        }
        
        @Override
        public void onReceivedHttpAuthRequest(WebView view, HttpAuthHandler handler, String host, String realm) {
        	super.onReceivedHttpAuthRequest(view, handler, host, realm);
        	Log.i("WebViewClient", "http auth");
        }
        
        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
//        	super.onReceivedSslError(view, handler, error);
        	Log.i("WebViewClient", "ssl error: " + error);
        	handler.proceed();
        }
        
        @Override
        public void onFormResubmission(WebView view, Message dontResend, Message resend) {
        	resend.sendToTarget();
        }
    }
    
    private class NattstadJavascriptInterface {
    	@JavascriptInterface
    	public void uploadFileAndroid(final int type, final int id) {
//    		Log.i("JavascriptInterface", "uploadFileAndroid called with type " + type + " and id " + id);
    		
    		final MainActivity activity = (MainActivity) getActivity();
    		activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					activity.chooseImageForUpload(type, id);
				}
			});
    	}
    	
    	@JavascriptInterface
    	public void goBack() {
    		final MainActivity activity = (MainActivity) getActivity();
    		activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					activity.onBackPressed();
				}
			});
    	}
    	
    	@JavascriptInterface
    	public void goBackRefresh() {
    		final MainActivity activity = (MainActivity) getActivity();
    		activity.runOnUiThread(new Runnable() {
				@Override
				public void run() {
					activity.onBackPressed();
		    		activity.refreshWebview();
				}
			});	
    	}
    }

	public void followBlogOnOff() {
		try {
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("memberid", blogUserId.toString()));
			HttpGet get = NattstadWebserviceCall.newGetRequest(getActivity(), isFollowingBlog ? NattstadWebserviceCall.Method.SetFollowOff : NattstadWebserviceCall.Method.SetFollowOn, params);
			new NattstadWebserviceCall<Boolean>(new BasicParsers.BoolParser(), new ResultCallback<Boolean>() {
				@Override
				public void handleResult(Boolean result, Exception exception) {
					if (result != null && exception == null) {
						Log.i("WebViewFragment", "follow/unfollow blog successful");
						if (isFollowingBlog != null) {
							isFollowingBlog = !isFollowingBlog;
						}
					}
				}
			}).execute(get);
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
	}

	public boolean shouldShowBlogOptionsMenu() {
		return blogUserId != null;
	}
	
	public boolean isFollowing() {
		return isFollowingBlog != null ? isFollowingBlog : false;
	}

	public String getUrl() {
		return webView.getUrl();
	}

	public Integer getBlogUserId() {
		return blogUserId;
	}

	public String getTitle() {
		return title;
	}
	
	public MenuOption getMenuOption() {
		return menuOption;
	}
}
