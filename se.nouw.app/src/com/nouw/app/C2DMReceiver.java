package com.nouw.app;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.message.BasicNameValuePair;

import com.nouw.app.MainActivity;
import com.nouw.app.BasicParsers.BoolParser;
import com.nouw.app.NattstadWebserviceCall.ResultCallback;

import com.nouw.app.R;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.os.Vibrator;
import android.util.Log;

public class C2DMReceiver extends BroadcastReceiver {

	
	public static final String C2DM_SENDER_URL = "837268829327";
	//public static final String C2DM_SENDER_URL = "75112091139";
	public static final String REGISTRATION_TOKEN_KEY = "se.nattstad.registration_token_key";

	private Context context;
	@Override
	public void onReceive(Context context, Intent intent) {
		this.context = context;
		if (intent.getAction().equals("com.google.android.c2dm.intent.REGISTRATION")) {
			handleRegistration(context, intent);
		} else if (intent.getAction().equals("com.google.android.c2dm.intent.RECEIVE")) {
			handleMessage(context, intent);
		}
	}

	private void sendRegistrationIDToServer(String registration) {
		try {
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("DeviceToken", registration));
			PackageInfo packageInfo = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			params.add(new BasicNameValuePair("AppVersion", packageInfo.versionName));
			
			HttpGet get = NattstadWebserviceCall.newGetRequest(context, NattstadWebserviceCall.Method.RegisterAndroidToken, params);
			new NattstadWebserviceCall<Boolean>(new BoolParser(), new ResultCallback<Boolean>() {
				@Override
				public void handleResult(Boolean success, Exception exception) {
					if (success != null && success == true) {
						Log.i("c2dm", "registration successful");
					}
					else {
						Log.i("c2dm", "registration failed");
					}
				}
			}).execute(get);
		} 
		catch (Exception e) {
			e.printStackTrace();
			Log.i("c2dm", "exception when trying to register: " + e.getMessage());
		}
	}

	private void handleRegistration(Context context, Intent intent) {
		String registration = intent.getStringExtra("registration_id");
		if (intent.getStringExtra("error") != null) {
			// Registration failed, should try again later.
			Log.d("c2dm", "registration failed");
			String error = intent.getStringExtra("error");
			if(error == "SERVICE_NOT_AVAILABLE"){
				Log.d("c2dm", "SERVICE_NOT_AVAILABLE");
			}else if(error == "ACCOUNT_MISSING"){
				Log.d("c2dm", "ACCOUNT_MISSING");
			}else if(error == "AUTHENTICATION_FAILED"){
				Log.d("c2dm", "AUTHENTICATION_FAILED");
			}else if(error == "TOO_MANY_REGISTRATIONS"){
				Log.d("c2dm", "TOO_MANY_REGISTRATIONS");
			}else if(error == "INVALID_SENDER"){
				Log.d("c2dm", "INVALID_SENDER");
			}else if(error == "PHONE_REGISTRATION_ERROR"){
				Log.d("c2dm", "PHONE_REGISTRATION_ERROR");
			}
		} else if (intent.getStringExtra("unregistered") != null) {
			// unregistration done, new messages from the authorized sender will be rejected
			Log.d("c2dm", "unregistered");
			/*registration = context.getSharedPreferences(Config.PREFERENCES_KEY, Context.MODE_PRIVATE).getString(Config.REGISTRATION_KEY, null);
	    	String username = context.getSharedPreferences(Config.PREFERENCES_KEY, Context.MODE_PRIVATE).getString(Config.USERNAME_KEY, null);
	    	if((registration != null) && (username != null)) {
	    		sendRegistrationIDToServerToUnregister(registration, username);
	    	}*/
		} else if (registration != null) {
			Log.d("c2dm", registration);
			Editor editor = context.getSharedPreferences(MainActivity.SHARED_PREFERENCES_KEY, 0).edit();
			editor.putString(REGISTRATION_TOKEN_KEY, registration);
			editor.commit();
			sendRegistrationIDToServer(registration);
		}
	}

	private void createNotification(Context context, String payload, int number, String header) {
		NotificationManager notificationManager = (NotificationManager) context
				.getSystemService(Context.NOTIFICATION_SERVICE);
		int icon = R.drawable.ic_notification;
		CharSequence tickerText = payload;
		long when = System.currentTimeMillis();

		Notification notification = new Notification(icon, tickerText, when);
		notification.flags |= Notification.FLAG_AUTO_CANCEL;
		notification.number = number;
		CharSequence contentTitle = context.getString(R.string.app_name);
		if(header != null) {
			contentTitle = header;
		} 
		CharSequence contentText = payload;

		Intent notificationIntent = new Intent(context, MainActivity.class);
		notificationIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		PendingIntent contentIntent = PendingIntent.getActivity(context, 0, notificationIntent, 0);
		notification.setLatestEventInfo(context, contentTitle, contentText, contentIntent);
		notificationManager.notify(1, notification);
	}

	private void handleMessage(Context context, Intent intent) {
		final String payload = intent.getStringExtra("payload");
		final String numberString = intent.getStringExtra("number");
		final String header = intent.getStringExtra("header");
		final String vibrate = intent.getStringExtra("vibrate");
		int number = 0;
		if (numberString != null) {
			number = Integer.parseInt(numberString);
		}
		Log.d("C2DM", "dmControl: payload = " + payload);
		Log.d("C2DM", "dmControl: number = " + number);
		Log.d("C2DM", "dmControl: header = " + header);
		Log.d("C2DM", "dmControl: vibrate = " + vibrate);
		if (number == 0) {
			NotificationManager notificationManager = (NotificationManager) context
					.getSystemService(Context.NOTIFICATION_SERVICE);
			notificationManager.cancelAll();
		}
		else {
			createNotification(context, payload, number, header);
			if (vibrate.toLowerCase().equals("true")) {
				Vibrator vibrator = (Vibrator) context.getSystemService(Context.VIBRATOR_SERVICE);
				vibrator.vibrate(new long[]{0, 500, 500, 500}, -1);
			}
		}
	}
}