package com.nouw.app;

import java.lang.ref.WeakReference;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.message.BasicNameValuePair;

import com.nouw.app.R;

import com.facebook.Session;
import com.nouw.app.BasicParsers.BoolParser;
import com.nouw.app.NattstadWebserviceCall.ErrorCode;
import com.nouw.app.NattstadWebserviceCall.Method;
import com.nouw.app.NattstadWebserviceCall.ResultCallback;
import com.nouw.app.SideMenuItem.MenuOption;

import android.media.ExifInterface;
import android.net.Uri;
import android.os.Bundle;
import android.os.Vibrator;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentManager.BackStackEntry;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.ActionBarActivity;
import android.util.Base64;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

public class MainActivity extends ActionBarActivity implements
		NavigationDrawerFragment.NavigationDrawerCallbacks {
	private final int LOGIN_INTENT_REQUEST = 100;
	private final int NEW_BLOG_POST_INTENT_REQUEST = 110;
	private final int CHOOSE_IMAGE_REQUEST = 120;
	public static final String SHARED_PREFERENCES_KEY = "se.nattstad.shared_preferences";

	private static final String CURRENT_MENU_OPTION_KEY = "se.nattstad.current_menu_option";
	private static final String CURRENT_FRAGMENT_TAG_KEY = "se.nattstad.current_fragment_tag";
	private NavigationDrawerFragment mNavigationDrawerFragment;
	private MenuOption currentMenuOption = MenuOption.None;
	private String currentFragmentTag = null;
	private List<WeakReference<Fragment>> allFragments = new ArrayList<WeakReference<Fragment>>();
	private ScheduledExecutorService backgroundUpdates = null;
	private int numUnreadMessages = 0;
	private int numUnreadNotifications = 0;
	private static final String NUM_UNREAD_MESSAGES_KEY = "se.nattstad.num_unread_messages";
	private static final String NUM_UNREAD_NOTIFICATIONS_KEY = "se.nattstad.num_unread_notifications";
	private static boolean running = false;

	private final Runnable checkMessagesAndNotificationsRunnable = new Runnable() {
		@Override
		public void run() {
			Log.i("MessageAndNotificationLoop",
					"checking for new messages/notifications");
			try {
				List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
				BasicNameValuePair pair = new BasicNameValuePair("lang_code",
						Locale.getDefault().getLanguage());
				params.add(pair);
				HttpGet get = NattstadWebserviceCall.newGetRequest(
						MainActivity.this,
						NattstadWebserviceCall.Method.GetCountNewMessages,
						params);
				final Integer numNewMessages = NattstadWebserviceCall.call(get,
						new BasicParsers.IntParser());

				get = NattstadWebserviceCall.newGetRequest(MainActivity.this,
						NattstadWebserviceCall.Method.GetCountNewNotifications,
						params);
				final Integer numNewNotifications = NattstadWebserviceCall
						.call(get, new BasicParsers.IntParser());
				runOnUiThread(new Runnable() {
					@Override
					public void run() {
						if (!running)
							return;

						final int prevNumMessages = numUnreadMessages;
						final int prevNumNotifications = numUnreadNotifications;
						if (numNewMessages != null) {
							Log.i("MessageAndNotificationLoop",
									"num unread messages "
											+ numNewMessages.toString());
							mNavigationDrawerFragment
									.setNumUnreadMessages(numNewMessages);
							numUnreadMessages = numNewMessages.intValue();
						}
						if (numNewNotifications != null) {
							Log.i("MessageAndNotificationLoop",
									"num unread notifications "
											+ numNewNotifications.toString());
							numUnreadNotifications = numNewNotifications
									.intValue();
						}

						if (numUnreadMessages > prevNumMessages) {
							Vibrator vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
							vibrator.vibrate(new long[] { 0, 500, 500, 500 },
									-1);
						}

						invalidateOptionsMenu();
					}
				});
			} catch (Exception e) {

			}
		}
	};
	private int imageUploadType;
	private int imageUploadId;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.i("MainActivity", "onCreate");
		setContentView(R.layout.activity_main);

		try {
			PackageInfo info = getPackageManager().getPackageInfo(
					"com.nouw.app", PackageManager.GET_SIGNATURES);
			for (Signature signature : info.signatures) {
				MessageDigest md = MessageDigest.getInstance("SHA");
				md.update(signature.toByteArray());
				String s = Base64.encodeToString(md.digest(), Base64.DEFAULT);
				Log.e("Your Tag",
						Base64.encodeToString(md.digest(), Base64.DEFAULT));
			}
		} catch (Exception e) {
			e.printStackTrace();
		}

		if (savedInstanceState != null) {
			currentMenuOption = (MenuOption) savedInstanceState
					.getSerializable(CURRENT_MENU_OPTION_KEY);
			currentFragmentTag = savedInstanceState
					.getString(CURRENT_FRAGMENT_TAG_KEY);
			fixRecreatedFragments();
			numUnreadMessages = savedInstanceState
					.getInt(NUM_UNREAD_MESSAGES_KEY);
			numUnreadNotifications = savedInstanceState
					.getInt(NUM_UNREAD_NOTIFICATIONS_KEY);
		}

		mNavigationDrawerFragment = (NavigationDrawerFragment) getSupportFragmentManager()
				.findFragmentById(R.id.navigation_drawer);

		mNavigationDrawerFragment.setUp(R.id.navigation_drawer,
				(DrawerLayout) findViewById(R.id.drawer_layout));

		SharedPreferences preferences = getSharedPreferences(
				SHARED_PREFERENCES_KEY, MODE_PRIVATE);

		if ((preferences.getString(LoginActivity.USERNAME_KEY, null) != null && preferences
				.getString(LoginActivity.PASSWORD_KEY, null) != null)
				|| isLoggedInToFacebook()) {
			registerForPushNotifications();
		} else {
			Intent loginIntent = new Intent(this, LoginActivity.class);
			startActivityForResult(loginIntent, LOGIN_INTENT_REQUEST);
			finish();
		}

		backgroundUpdates = Executors.newScheduledThreadPool(1);
		backgroundUpdates.scheduleAtFixedRate(
				checkMessagesAndNotificationsRunnable, 0, 30, TimeUnit.SECONDS);
	}

	@Override
	public void onAttachFragment(Fragment fragment) {
		super.onAttachFragment(fragment);
		allFragments.add(new WeakReference<Fragment>(fragment));
	}

	private List<Fragment> getActiveFragments() {
		ArrayList<Fragment> ret = new ArrayList<Fragment>();
		for (WeakReference<Fragment> ref : allFragments) {
			Fragment f = ref.get();
			if (f != null && f.isVisible()) {
				ret.add(f);
			}
		}
		return ret;
	}

	private void fixRecreatedFragments() {
		FragmentManager fragmentManager = getSupportFragmentManager();
		FragmentTransaction transaction = fragmentManager.beginTransaction();
		for (Fragment f : getActiveFragments()) {
			if (f.getTag() != currentFragmentTag
					&& f instanceof WebViewFragment) {
				transaction.hide(f);
			} else {
				transaction.show(f);
			}
		}
		transaction.commit();
	}

	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putSerializable(CURRENT_MENU_OPTION_KEY, currentMenuOption);
		outState.putString(CURRENT_FRAGMENT_TAG_KEY, currentFragmentTag);
		outState.putInt(NUM_UNREAD_MESSAGES_KEY, numUnreadMessages);
		outState.putInt(NUM_UNREAD_NOTIFICATIONS_KEY, numUnreadNotifications);
	}

	@Override
	protected void onDestroy() {
		Log.i("MainActivity", "onDestroy");
		super.onDestroy();
	}

	@Override
	protected void onPause() {
		super.onPause();
		running = false;
		if (backgroundUpdates != null) {
			backgroundUpdates.shutdownNow();
			backgroundUpdates = null;
		}
		Log.i("MainActivity", "onPause");
	}

	@Override
	protected void onResume() {
		super.onResume();
		Log.i("MainActivity", "onResume");
		running = true;
		backgroundUpdates = Executors.newScheduledThreadPool(1);
		backgroundUpdates.scheduleAtFixedRate(
				checkMessagesAndNotificationsRunnable, 0, 30, TimeUnit.SECONDS);
		// fixRecreatedFragments();

		restoreActionBar();
		// if (mNavigationDrawerFragment.isDrawerOpen()) {
		// Log.i("MainActivity", "drawer open in onResume");
		// mNavigationDrawerFragment.closeDrawer();
		// mNavigationDrawerFragment.setDrawerEnabled(true);
		// }
	}

	private boolean isLoggedInToFacebook() {
		Session session = Session.openActiveSessionFromCache(this);
		return session != null && session.isOpened();
	}

	private void registerForPushNotifications() {
		Intent registrationIntent = new Intent(
				"com.google.android.c2dm.intent.REGISTER");
		registrationIntent.putExtra("app",
				PendingIntent.getBroadcast(this, 0, new Intent(), 0));
		registrationIntent.putExtra("sender", C2DMReceiver.C2DM_SENDER_URL);
		startService(registrationIntent);
	}

	private void unregisterForPushNotifications() {
		SharedPreferences sharedPreferences = getSharedPreferences(
				SHARED_PREFERENCES_KEY, 0);
		Editor editor = sharedPreferences.edit();
		String registrationToken = sharedPreferences.getString(
				C2DMReceiver.REGISTRATION_TOKEN_KEY, "");
		editor.remove(C2DMReceiver.REGISTRATION_TOKEN_KEY);
		editor.commit();

		try {
			List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
			params.add(new BasicNameValuePair("DeviceToken", registrationToken));
			BasicNameValuePair pair = new BasicNameValuePair("lang_code",
					Locale.getDefault().getLanguage());
			params.add(pair);
			HttpGet get = NattstadWebserviceCall.newGetRequest(this,
					NattstadWebserviceCall.Method.UnregisterAndroidToken,
					params);
			new NattstadWebserviceCall<Boolean>(new BoolParser(),
					new ResultCallback<Boolean>() {
						@Override
						public void handleResult(Boolean result,
								Exception exception) {
							if (result != null && result == true
									&& exception == null) {
								Log.i("MainActivity",
										"unregistering for push notifications successful");
							}
						}
					}).execute(get);
		} catch (Exception e) {

		}
	}

	@Override
	public void onBackPressed() {
		FragmentManager fragmentManager = getSupportFragmentManager();
		if (fragmentManager.getBackStackEntryCount() == 1) {
			mNavigationDrawerFragment.setDrawerEnabled(true);
		}
		WebViewFragment webViewFragment = (WebViewFragment) fragmentManager
				.findFragmentByTag(currentFragmentTag);
		if (webViewFragment == null || !webViewFragment.goBackIfPossible()) {
			if (fragmentManager.getBackStackEntryCount() > 0) {
				BackStackEntry entry = fragmentManager
						.getBackStackEntryAt(fragmentManager
								.getBackStackEntryCount() - 1);
				currentFragmentTag = entry.getName();
			}
			super.onBackPressed();
		}

		invalidateOptionsMenu();
	}

	@Override
	public void onNavigationDrawerItemSelected(int position) {
		SideMenuItem menuItem = SideMenuItem.getItemList().get(position);
		handleMenuItemSelection(menuItem);
	}

	private void handleMenuItemSelection(SideMenuItem menuItem) {
		MenuOption option = menuItem.option;

		if (currentMenuOption != option) {
			switch (option) {
			case Inbox:
			case Newsfeed:
			case Magazine:
			case Following:
			case AllBlogPosts:
			case Comments:
			case Settings:
			case BlogDesign:
			case Statistics:
			case BlogTopList:
			case MyProfile:
			case MyBlog:
			case MyPictures:
			case AccountInfo:
			case Blocklist:
			case Notifications:
				FragmentManager fragmentManager = getSupportFragmentManager();
				WebViewFragment webView = (WebViewFragment) fragmentManager
						.findFragmentByTag(option.toString());
				if (webView == null) {
					webView = WebViewFragment.newInstance(option, null);
					FragmentTransaction transaction = fragmentManager
							.beginTransaction();
					Fragment previous = fragmentManager
							.findFragmentByTag(currentMenuOption.toString());
					transaction.add(R.id.container, webView, option.toString());
					if (previous != null) {
						transaction.hide(previous);
					}
					transaction.commit();
				} else {
					FragmentTransaction transaction = fragmentManager
							.beginTransaction();
					Fragment previous = fragmentManager
							.findFragmentByTag(currentMenuOption.toString());
					transaction.show(webView);
					if (previous != null) {
						transaction.hide(previous);
					}
					transaction.commit();
					webView.updatePage();
				}
				setActionBarTitle(menuItem.title);
				currentMenuOption = option;
				currentFragmentTag = option.toString();
				break;
			case NewBlogPost:
				Intent newBlogPost = new Intent(this, PostBlogActivity.class);
				startActivityForResult(newBlogPost,
						NEW_BLOG_POST_INTENT_REQUEST);
				break;
			case LogOut:
				unregisterForPushNotifications();

				Session session = Session.getActiveSession();
				if (session != null)
					session.closeAndClearTokenInformation();
				Editor editor = getSharedPreferences(SHARED_PREFERENCES_KEY,
						MODE_PRIVATE).edit();
				editor.remove(LoginActivity.USERNAME_KEY);
				editor.remove(LoginActivity.PASSWORD_KEY);
				editor.remove(LoginActivity.NICKNAME_KEY);
				editor.commit();

				Intent loginIntent = new Intent(this, LoginActivity.class);
				startActivity(loginIntent);
				finish();
				break;
			}
		} else {
			FragmentManager fragmentManager = getSupportFragmentManager();
			WebViewFragment webView = (WebViewFragment) fragmentManager
					.findFragmentByTag(option.toString());
			if (webView != null) {
				webView.updatePage();
			}
		}

		invalidateOptionsMenu();
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);

		if (requestCode == NEW_BLOG_POST_INTENT_REQUEST
				&& resultCode == RESULT_OK) {
			mNavigationDrawerFragment.selectMenuOption(MenuOption.AllBlogPosts);
		} else if (requestCode == CHOOSE_IMAGE_REQUEST
				&& resultCode == RESULT_OK) {
			final ProgressDialog dialog = ProgressDialog.show(this, "",
					"Laddar upp bild...", true);
			Uri imageToUpload = data.getData();
			try {
				Bundle body = new Bundle();
				body.putByteArray("upload",
						Utility.createJPEG(this, imageToUpload, 1960, 1960));
				body.putString("type", Integer.toString(imageUploadType));
				body.putString("id", Integer.toString(imageUploadId));
				int orientation = Utility.getOrientationOfImage(this,
						imageToUpload);
				if (orientation == ExifInterface.ORIENTATION_ROTATE_90) {
					body.putString("orientation", Integer
							.toString(ExifInterface.ORIENTATION_ROTATE_270));
				} else if (orientation == ExifInterface.ORIENTATION_ROTATE_270) {
					body.putString("orientation", Integer
							.toString(ExifInterface.ORIENTATION_ROTATE_90));
				} else {
					body.putString("orientation", Integer.toString(orientation));
				}
				HttpPost post = NattstadWebserviceCall.newPostRequest(this,
						body, Method.PostPictureAndroid);
				new NattstadWebserviceCall<Boolean>(new BoolParser(),
						new ResultCallback<Boolean>() {
							@Override
							public void handleResult(Boolean result,
									Exception exception) {
								dialog.dismiss();
								FragmentManager fragmentManager = getSupportFragmentManager();
								WebViewFragment webView = (WebViewFragment) fragmentManager
										.findFragmentByTag(currentFragmentTag);
								if (webView != null)
									webView.updatePage();

								if (result != null
										&& result.booleanValue() == true
										&& exception == null) {
									Log.i("MainActivity", "upload successful");
								} else {
									Log.i("MainActivity", "upload failed");
									if (exception != null) {
										if (exception instanceof ErrorCode) {
											ErrorCode e = (ErrorCode) exception;
											Log.i("MainActivity",
													"returned with status code "
															+ Integer.toString(e
																	.getErrorCode()));
										} else {
											Log.i("MainActivity",
													exception.getMessage());
										}
									}
								}
							}
						}).execute(post);
			} catch (Exception e) {
				dialog.dismiss();
			}
		}
	}

	private void setActionBarTitle(String title) {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setTitle(title);
	}

	public void addNewWebViewFragment(String url) {
		mNavigationDrawerFragment.setDrawerEnabled(false);

		FragmentManager fragmentManager = getSupportFragmentManager();
		WebViewFragment webView = WebViewFragment.newInstance(MenuOption.None,
				url);
		FragmentTransaction transaction = fragmentManager.beginTransaction();

		Fragment previous = fragmentManager
				.findFragmentByTag(currentFragmentTag);
		if (previous != null) {
			transaction.hide(previous);
			transaction.addToBackStack(currentFragmentTag);
		}

		currentFragmentTag = url;

		transaction.add(R.id.container, webView, url);
		transaction.commit();

		invalidateOptionsMenu();
	}

	public void onSectionAttached(int number) {

	}

	public void restoreActionBar() {
		ActionBar actionBar = getSupportActionBar();
		actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_STANDARD);
		actionBar.setDisplayShowTitleEnabled(true);
		actionBar.setHomeButtonEnabled(true);
		actionBar.setDisplayShowHomeEnabled(true);

		FragmentManager fragmentManager = getSupportFragmentManager();
		WebViewFragment currentFragment = (WebViewFragment) fragmentManager
				.findFragmentByTag(currentFragmentTag);
		if (currentFragment != null) {
			String title = currentFragment.getTitle();
			actionBar.setTitle(title);
		}
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		if (mNavigationDrawerFragment == null
				|| !mNavigationDrawerFragment.isDrawerOpen()) {
			// Only show items in the action bar relevant to this screen
			// if the drawer is not showing. Otherwise, let the drawer
			// decide what to show in the action bar.

			FragmentManager fragmentManager = getSupportFragmentManager();
			WebViewFragment fragment = (WebViewFragment) fragmentManager
					.findFragmentByTag(currentFragmentTag);

			if (fragment != null && fragment.shouldShowBlogOptionsMenu()) {
				getMenuInflater().inflate(R.menu.visit_blog, menu);
			} else {
				getMenuInflater().inflate(R.menu.main, menu);
			}
			restoreActionBar();
			return true;
		}
		return super.onCreateOptionsMenu(menu);
	}

	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		MenuItem menuItem = menu.findItem(R.id.action_follow);
		if (menuItem != null) {
			FragmentManager fragmentManager = getSupportFragmentManager();
			WebViewFragment fragment = (WebViewFragment) fragmentManager
					.findFragmentByTag(currentFragmentTag);
			menuItem.setTitle(fragment.isFollowing() ? R.string.unfollow
					: R.string.follow);
		}

		MenuItem notificationItem = menu.findItem(R.id.action_notifications);
		if (notificationItem != null) {
			int badgeNumber = Math.min(numUnreadNotifications, 10);
			int id = getResources().getIdentifier(
					"notifications" + Integer.toString(badgeNumber),
					"drawable", getPackageName());
			notificationItem.setIcon(id);
		}

		return super.onPrepareOptionsMenu(menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		FragmentManager fragmentManager = getSupportFragmentManager();
		WebViewFragment currentFragment = (WebViewFragment) fragmentManager
				.findFragmentByTag(currentFragmentTag);

		switch (item.getItemId()) {
		case android.R.id.home:
			if (!mNavigationDrawerFragment.isDrawerEnabled()) {
				onBackPressed();
				return true;
			} else {
				return super.onOptionsItemSelected(item);
			}
		case R.id.action_notifications:
			numUnreadNotifications = 0;
			addNewWebViewFragment(currentFragment
					.createUrl(MenuOption.Notifications));
			return true;

		case R.id.action_follow:
			currentFragment.followBlogOnOff();
			return true;

		case R.id.action_visit_profile:
			visitProfile(currentFragment.getBlogUserId(),
					currentFragment.getTitle());
			return true;

		case R.id.action_open_external:
			String externalUrl = removeAppSpecificGetParameters(
					currentFragment.getUrl()).replace("https://m2",
					"http://www");
			Intent externalBrowser = new Intent(Intent.ACTION_VIEW,
					Uri.parse(externalUrl));
			startActivity(externalBrowser);
			return true;

		case R.id.action_copy_url:
			ClipboardManager clipboard = (ClipboardManager) getSystemService(Context.CLIPBOARD_SERVICE);
			clipboard.setPrimaryClip(ClipData.newPlainText("url",
					removeAppSpecificGetParameters(currentFragment.getUrl())
							.replace("https://m2", "http://www")));
			return true;
		}
		return super.onOptionsItemSelected(item);
	}

	private void visitProfile(Integer blogUserId, String title) {
		if (blogUserId == null)
			return;

		Uri.Builder builder = Uri.parse(
				WebViewFragment.WEBVIEW_BASE_URL + "visit_presentation.aspx")
				.buildUpon();
		builder.appendQueryParameter("id", blogUserId.toString());
		builder.appendQueryParameter("nstitle", title);
		addNewWebViewFragment(builder.build().toString());
	}

	private String removeAppSpecificGetParameters(String url) {
		Uri uri = Uri.parse(url);

		String[] parts = url.split("\\?");
		if (parts.length > 0) {
			Uri.Builder builder = Uri.parse(parts[0]).buildUpon();
			for (String key : uri.getQueryParameterNames()) {
				if (key.equals("nstitle") || key.equals("nsuserid")
						|| key.equals("nssamepage") || key.equals("app")
						|| key.equals("lang_code")) {
					// if (key.equals("nstitle") || key.equals("nsuserid") ||
					// key.equals("nssamepage") || key.equals("app")) {
					continue;
				}
				builder.appendQueryParameter(key, uri.getQueryParameter(key));
			}
			return builder.build().toString();
		}

		return null;
	}

	public void updateBlogMenu(boolean isFollowing) {
		restoreActionBar();
		invalidateOptionsMenu();
	}

	public void chooseImageForUpload(int type, int id) {
		imageUploadType = type;
		imageUploadId = id;
		Intent intent = new Intent(Intent.ACTION_PICK,
				android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
		startActivityForResult(intent, CHOOSE_IMAGE_REQUEST);
	}

	public void refreshWebview() {
		FragmentManager fragmentManager = getSupportFragmentManager();
		WebViewFragment currentFragment = (WebViewFragment) fragmentManager
				.findFragmentByTag(currentFragmentTag);
		currentFragment.updatePage();
	}
}
