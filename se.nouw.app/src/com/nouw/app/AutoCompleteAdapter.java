package com.nouw.app;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.message.BasicNameValuePair;

import com.nouw.app.NattstadWebserviceCall.ResultParser;


import android.content.Context;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.Filter;
import android.widget.Filterable;

public class AutoCompleteAdapter extends ArrayAdapter<String> implements Filterable {
    ArrayList<Person> items;
    Context context;

    public AutoCompleteAdapter(Context context, int textViewResourceId) {
        super(context, textViewResourceId);

        items = new ArrayList<Person>();
        this.context = context;
    }

    @Override
    public int getCount() {
        return items.size();
    }

    @Override
    public String getItem(int idx) {
        return items.get(idx).nickname;
    }

    @Override
    public Filter getFilter() {
        Filter myFilter = new Filter() {
            private String responseToString(HttpResponse response) throws IllegalStateException, IOException{
                String result = "";
                InputStream in = response.getEntity().getContent();
                BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                StringBuilder str = new StringBuilder();
                String line = null;
                while((line = reader.readLine()) != null){
                    str.append(line + "\n");
                }
                in.close();
                result = str.toString();
                return result;
            }

            @Override
            protected FilterResults performFiltering(CharSequence constraint) {
                FilterResults filterResults = new FilterResults();
                if(constraint != null) {
                    Log.i("arrayadapter filter", constraint.toString());

                    try {
                		List<BasicNameValuePair> params = new ArrayList<BasicNameValuePair>();
                		params.add(new BasicNameValuePair("SearchName", constraint.toString()));
                		params.add(new BasicNameValuePair("android", "false"));
                		
                		HttpGet get = NattstadWebserviceCall.newGetRequest(context, NattstadWebserviceCall.Method.GetUsersbyName, params);
                		
                		List<Person> persons = NattstadWebserviceCall.call(get, new ResultParser<List<Person>>() {
                			@Override
                			public List<Person> parseResult(InputStream in) throws Exception {
                				return Person.personsFromXml(in);
                			}
						});
                		
                        for (Person p : persons) {
                            Log.i("person", p.nickname);
                        }

                        filterResults.values = persons;
                        filterResults.count = persons.size();

                        Log.i("xml result", "number of persons parsed " + Integer.toString(persons.size()));
                    }
                    catch (Exception e) {
						e.printStackTrace();
					}
                }
                return filterResults;
            }

            @SuppressWarnings("unchecked")
			@Override
            protected void publishResults(CharSequence constraint, FilterResults results) {
                if(results != null && results.count > 0) {
                    items = (ArrayList<Person>) results.values;
                    notifyDataSetChanged();
                }
                else {
                    notifyDataSetInvalidated();
                }
            }
        };
        return myFilter;
    }
}
