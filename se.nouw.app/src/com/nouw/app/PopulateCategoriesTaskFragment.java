package com.nouw.app;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import org.apache.http.message.BasicNameValuePair;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;

import com.nouw.app.R;

import android.app.Activity;
import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;

public class PopulateCategoriesTaskFragment extends Fragment {		
	public static interface TaskCallback {
		void onPreExecute();
	    void onPostExecute(NodeList result);
	}
	
	private PopulateCategoriesTask task;
	private TaskCallback owner;
	private Context context;
	
	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(true);
		task = new PopulateCategoriesTask();
		task.execute();
	}
	
	@Override
	public void onAttach(Activity activity) {
		super.onAttach(activity);
		owner = (TaskCallback)activity;
		context = activity;
	}
	
	@Override
	public void onDetach() {
		super.onDetach();
		owner = null;
		context = null;
	}
	
	private class PopulateCategoriesTask extends AsyncTask<Void, Void, NodeList> {
		@Override
		protected void onPreExecute() {
			super.onPreExecute();
			if (owner != null) {
				owner.onPreExecute();
			}
		}
		
		@Override
		protected NodeList doInBackground(Void... params) {
			try {
				List<BasicNameValuePair> params2 = new ArrayList<BasicNameValuePair>();
				BasicNameValuePair pair = new BasicNameValuePair("lang_code",Locale.getDefault().getLanguage());
				params2.add(pair);
				String url = NattstadWebserviceCall.newGetRequest(context, NattstadWebserviceCall.Method.GetBlogCategories, params2).getURI().toString();
				final String xml = XMLParser.getXmlFromUrl(url);
				return parseBlogCategoryXML(xml);
			} 
			catch (Exception e) {
				return null;
			}
		}
		
		@Override
		protected void onPostExecute(NodeList result) {
			super.onPostExecute(result);
			if (owner != null) {
				owner.onPostExecute(result);
			}
		}
		
		private NodeList parseBlogCategoryXML(String xml) throws Exception {
			if(xml != null) {
				Document doc = XMLParser.getDomElement(xml);
				if(doc != null) {
					NodeList nl = doc.getElementsByTagName("BlogCategory");
					if(nl != null) {
						return nl;
					}
				}
			}
			throw new Exception(context.getResources().getString(R.string.nattstad_unreachable));
		}
	}
}