package com.nouw.app;

import android.util.Xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Erik on 2013-12-11.
 */
public class Person {
    public String nickname;
    public String imgUrl;
    public String firstName;
    public int memberId;

    private static final String xmlNamespace = null;

    public Person(String nickname, String imgUrl, String firstName, int memberId) {
        this.nickname = nickname;
        this.imgUrl = imgUrl;
        this.firstName = firstName;
        this.memberId = memberId;
    }

    public static List<Person> personsFromXml(InputStream xmlData) throws IOException, XmlPullParserException {
        XmlPullParser parser = Xml.newPullParser();

        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(xmlData, null);
        parser.nextTag();

        return readPersons(parser);
    }

    private static List<Person> readPersons(XmlPullParser parser) throws IOException, XmlPullParserException {
        List<Person> persons = new ArrayList<Person>();

        parser.require(XmlPullParser.START_TAG, xmlNamespace, "ArrayOfVisitor");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("Visitor")) {
                persons.add(readPerson(parser));
            } else {
                skip(parser);
            }
        }

        return persons;
    }

    private static Person readPerson(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, xmlNamespace, "Visitor");
        String nickname = null;
        String imgUrl = null;
        String firstName = null;
        int memberId = 0;

        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals("Smeknamn")) {
                nickname = readNickname(parser);
            }
            else if (name.equals("PresBild")) {
                imgUrl = readImageUrl(parser);
            }
            else if (name.equals("Fornamn")) {
                firstName = readFirstName(parser);
            }
            else if (name.equals("MedlemsID")) {
                memberId = readMemberId(parser);
            }
            else {
                skip(parser);
            }
        }

        return new Person(nickname, imgUrl, firstName, memberId);
    }

    private static void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
        if (parser.getEventType() != XmlPullParser.START_TAG) {
            throw new IllegalStateException();
        }
        int depth = 1;
        while (depth != 0) {
            switch (parser.next()) {
                case XmlPullParser.END_TAG:
                    depth--;
                    break;
                case XmlPullParser.START_TAG:
                    depth++;
                    break;
            }
        }

    }

    private static int readMemberId(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, xmlNamespace, "MedlemsID");
        int firstName = Integer.parseInt(readText(parser));
        parser.require(XmlPullParser.END_TAG, xmlNamespace, "MedlemsID");
        return firstName;
    }

    private static String readFirstName(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, xmlNamespace, "Fornamn");
        String firstName = readText(parser);
        parser.require(XmlPullParser.END_TAG, xmlNamespace, "Fornamn");
        return firstName;
    }

    private static String readImageUrl(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, xmlNamespace, "PresBild");
        String imageUrl = readText(parser);
        parser.require(XmlPullParser.END_TAG, xmlNamespace, "PresBild");
        return imageUrl;
    }

    private static String readNickname(XmlPullParser parser) throws IOException, XmlPullParserException {
        parser.require(XmlPullParser.START_TAG, xmlNamespace, "Smeknamn");
        String nickname = readText(parser);
        parser.require(XmlPullParser.END_TAG, xmlNamespace, "Smeknamn");
        return nickname;
    }

    private static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }
}
