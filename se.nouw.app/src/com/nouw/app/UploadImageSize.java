package com.nouw.app;

import java.io.IOException;
import java.io.InputStream;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import android.util.Xml;

public class UploadImageSize {
	public final int normalWidth;
	public final int slowNetworkWidth;
	private static final String NORMAL_WIDTH_TAG = "NormalWidth";
	private static final String SLOW_NETWORK_WIDTH_TAG = "SlowNetworkWidth";
	
	public UploadImageSize(int normalWidth, int slowNetworkWidth) {
		this.normalWidth = normalWidth;
		this.slowNetworkWidth = slowNetworkWidth;
	}
	
	public static UploadImageSize fromXml(InputStream in) throws XmlPullParserException, IOException {
		XmlPullParser parser = Xml.newPullParser();

        parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
        parser.setInput(in, null);
        parser.nextTag();
        
        int normalWidth = 0;
        int slowNetworkWidth = 0;
        parser.require(XmlPullParser.START_TAG, null, "UploadImageSize");
        while (parser.next() != XmlPullParser.END_TAG) {
            if (parser.getEventType() != XmlPullParser.START_TAG) {
                continue;
            }
            String name = parser.getName();
            if (name.equals(NORMAL_WIDTH_TAG)) {
            	parser.require(XmlPullParser.START_TAG, null, NORMAL_WIDTH_TAG);
            	normalWidth = Integer.parseInt(readText(parser));
                parser.require(XmlPullParser.END_TAG, null, NORMAL_WIDTH_TAG); 
            } 
            else if (name.equals(SLOW_NETWORK_WIDTH_TAG)) {
            	parser.require(XmlPullParser.START_TAG, null, SLOW_NETWORK_WIDTH_TAG);
            	slowNetworkWidth = Integer.parseInt(readText(parser));
                parser.require(XmlPullParser.END_TAG, null, SLOW_NETWORK_WIDTH_TAG); 
            }
        }
        
		return new UploadImageSize(normalWidth, slowNetworkWidth);
	}
	
	private static String readText(XmlPullParser parser) throws IOException, XmlPullParserException {
        String result = "";
        if (parser.next() == XmlPullParser.TEXT) {
            result = parser.getText();
            parser.nextTag();
        }
        return result;
    }
}
